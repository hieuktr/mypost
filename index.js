/** @format */

import {AppRegistry} from 'react-native';
import index from './js/index';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => index);
