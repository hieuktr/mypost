import { put, call, takeEvery, select } from 'redux-saga/effects';
import {
  fetchMessagesSucceeded, fetchMessagesFailed,
  postMessageSucceeded, postMessageFailed,
} from './actions';
import {
  FETCH_MESSAGES,
  POST_MESSAGE,
} from './constants';
import {
  fetchMessages,
  postMessage,
} from './apis';
import { tokenSelector } from '../auth/selectors';

function* fetchMessagesSideEffect(action) {
  try {
    const token = yield select(tokenSelector);
    const { messages } = yield call(fetchMessages, token, action.itemCode);
    yield put(fetchMessagesSucceeded(messages));
  } catch (e) {
    yield put(fetchMessagesFailed(e.message));
  }
}

function* postMessageSideEffect(action) {
  try {
    const token = yield select(tokenSelector);
    yield call(postMessage, token, action.message.itemCode, action.message.content);
    yield put(postMessageSucceeded(action.message));
  } catch (e) {
    yield put(postMessageFailed(e.message));
  }
}

export default function* notificationSaga() {
  yield takeEvery(FETCH_MESSAGES, fetchMessagesSideEffect);
  yield takeEvery(POST_MESSAGE, postMessageSideEffect);
}
