
import { APP_API_URL } from '../../config/app';

export function fetchMessages(token, itemCode) {
  return fetch(`${APP_API_URL}/ListPhanHoi?itemcode=${itemCode}`, {
    method: "GET",
    headers: {
      token,
    },
  })
    .then(response => response.json())
    .then(json => {
      console.log(json)
      if (json.status) {
        return {
          messages: json.phanhois || [],
        }
      } else {
        throw new Error(json.msg)
      }
    });
}

export function postMessage(token, itemCode, content) {
  return fetch(`${APP_API_URL}/CreatePhanHoi`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      token,
    },
    body: JSON.stringify({
      content,
      itemcode: itemCode,
    }),
  })
    .then(response => response.json())
    .then(json => {
      if (!json.status) {
        throw new Error(json.msg)
      }
    });
}
