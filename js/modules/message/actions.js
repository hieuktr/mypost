import {
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCEEDED,
  FETCH_MESSAGES_FAILED,
  POST_MESSAGE,
  POST_MESSAGE_SUCCEEDED,
  POST_MESSAGE_FAILED,
} from './constants';

export function fetchMessages(itemCode) {
  return {
    type: FETCH_MESSAGES,
    itemCode,
  };
}

export function fetchMessagesSucceeded(messages) {
  return {
    type: FETCH_MESSAGES_SUCCEEDED,
    messages,
  };
}

export function fetchMessagesFailed(error) {
  return {
    type: FETCH_MESSAGES_FAILED,
    error,
  };
}

export function postMessage(message) {
  return {
    type: POST_MESSAGE,
    message,
  };
}

export function postMessageSucceeded(message) {
  return {
    type: POST_MESSAGE_SUCCEEDED,
    message,
  };
}

export function postMessageFailed(error) {
  return {
    type: POST_MESSAGE_FAILED,
    error,
  };
}
