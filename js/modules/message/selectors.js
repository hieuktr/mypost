import { createSelector } from 'reselect';

const messageSelector = state => state.message;

export const messagesSelector = createSelector(
  messageSelector,
  messageReducer => messageReducer.get('messages').toJS(),
);

export const fetchMessagesLoadingSelector = createSelector(
  messageSelector,
  messageReducer => messageReducer.get('loading'),
);

export const fetchMessagesErrorSelector = createSelector(
  messageSelector,
  messageReducer => messageReducer.get('error'),
);

export const postMessageLoadingSelector = createSelector(
  messageSelector,
  messageReducer => messageReducer.get('postMessageLoading'),
);

export const postMessageErrorSelector = createSelector(
  messageSelector,
  messageReducer => messageReducer.get('postMessageError'),
);
