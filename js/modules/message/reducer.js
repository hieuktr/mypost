import { fromJS } from 'immutable';
import {
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCEEDED,
  FETCH_MESSAGES_FAILED,
  POST_MESSAGE,
  POST_MESSAGE_SUCCEEDED,
  POST_MESSAGE_FAILED,
} from './constants';

const initState = fromJS({
  messages: [],
  loading: false,
  error: '',
  postMessageLoading: false,
  postMessageError: '',
});

export default function messageReducer(state = initState, action = {}) {
  switch (action.type) {
    case FETCH_MESSAGES:
      return state
        .set('loading', true)
        .set('error', '');
    case FETCH_MESSAGES_SUCCEEDED:
      return state
        .set('loading', false)
        .set('messages', fromJS(action.messages));
    case FETCH_MESSAGES_FAILED:
      return state
        .set('loading', false)
        .set('error', action.error);

    case POST_MESSAGE:
      return state
        .set('postMessageLoading', true)
        .set('postMessageError', '');
    case POST_MESSAGE_SUCCEEDED:
      return state
        .set('postMessageLoading', false)
        .set('messages', state.get('messages').unshift(fromJS(action.message)));
    case POST_MESSAGE_FAILED:
      return state
        .set('postMessageLoading', false)
        .set('postMessageError', action.error);
    default:
      return state;
  }
}
