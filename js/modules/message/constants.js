export const FETCH_MESSAGES = 'message/FETCH_MESSAGES';
export const FETCH_MESSAGES_SUCCEEDED = 'message/FETCH_MESSAGES_SUCCEEDED';
export const FETCH_MESSAGES_FAILED = 'message/FETCH_MESSAGES_FAILED';

export const POST_MESSAGE = 'message/POST_MESSAGE';
export const POST_MESSAGE_SUCCEEDED = 'message/POST_MESSAGE_SUCCEEDED';
export const POST_MESSAGE_FAILED = 'message/POST_MESSAGE_FAILED';
