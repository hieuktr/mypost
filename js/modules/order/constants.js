export const FETCH_ORDERS = 'order/FETCH_ORDERS';
export const FETCH_ORDERS_SUCCEEDED = 'order/FETCH_ORDERS_SUCCEEDED';
export const FETCH_ORDERS_FAILED = 'order/FETCH_ORDERS_FAILED';

export const ADD_ORDER = 'order/ADD_ORDER';
