import { createSelector } from 'reselect';

const orderSelector = state => state.order;

export const ordersSelector = createSelector(
  orderSelector,
  orderReducer => orderReducer.get('orders').toJS(),
);

export const sendOrdersSelector = createSelector(
  orderSelector,
  orderReducer => orderReducer.get('sendOrders').toJS(),
);

export const receiveOrdersSelector = createSelector(
  orderSelector,
  orderReducer => orderReducer.get('receiveOrders').toJS(),
);

export const fetchOrdersLoadingSelector = createSelector(
  orderSelector,
  orderReducer => orderReducer.get('loading'),
);

export const fetchOrdersErrorSelector = createSelector(
  orderSelector,
  orderReducer => orderReducer.get('error'),
);
