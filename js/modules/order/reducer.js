import { fromJS } from 'immutable';
import {
  FETCH_ORDERS,
  FETCH_ORDERS_SUCCEEDED,
  FETCH_ORDERS_FAILED,
  ADD_ORDER,
} from './constants';

const initState = fromJS({
  loading: false,
  error: '',
  orders: [],
  sendOrders: [],
  receiveOrders: []
});

export default function orderReducer(state = initState, action = {}) {
  switch (action.type) {
    case FETCH_ORDERS:
      return state
        .set('loading', true)
        .set('error', '');
    case FETCH_ORDERS_SUCCEEDED:
      return state
        .set('loading', false)
        .set('sendOrders', fromJS(action.sendOrders))
        .set('receiveOrders', fromJS(action.receiveOrders));
    case FETCH_ORDERS_FAILED:
      return state
        .set('loading', false)
        .set('error', action.error);

    case ADD_ORDER:
      return state
        .set('orders', state.get('orders').unshift(fromJS(action.order)));
    default:
      return state;
  }
}
