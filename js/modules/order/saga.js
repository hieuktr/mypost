import { put, call, takeEvery, select } from 'redux-saga/effects';
import { fetchOrdersSucceeded, fetchOrdersFailed } from './actions';
import { FETCH_ORDERS } from './constants';
import { fetchOrders } from './apis';
import { tokenSelector } from '../auth/selectors';

function* fetchOrdersSideEffect() {
  try {
    const token = yield select(tokenSelector);
    const { sendOrders, receiveOrders } = yield call(fetchOrders, token);
    yield put(fetchOrdersSucceeded(sendOrders, receiveOrders));
  } catch (e) {
    yield put(fetchOrdersFailed(e.message));
  }
}

export default function* categorySaga() {
  yield takeEvery(FETCH_ORDERS, fetchOrdersSideEffect);
}
