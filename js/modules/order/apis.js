
import { APP_API_URL } from '../../config/app';

export function fetchOrders(token) {
  return fetch(`${APP_API_URL}/ListBuuGui`, {
    method: "GET",
    headers: {
      token,
    },
  })
    .then(response => response.json())
    .then(json => {
      if (json.status) {
        return {
          sendOrders: json.send || [],
          receiveOrders: json.receive || [],
        }
      } else {
        throw new Error(json.msg)
      }
    });
}
