import {
  FETCH_ORDERS,
  FETCH_ORDERS_SUCCEEDED,
  FETCH_ORDERS_FAILED,
  ADD_ORDER,
} from './constants';

export function fetchOrders() {
  return {
    type: FETCH_ORDERS,
  };
}

export function fetchOrdersSucceeded(sendOrders=[], receiveOrders=[]) {
  return {
    type: FETCH_ORDERS_SUCCEEDED,
    sendOrders,
    receiveOrders,
  };
}

export function fetchOrdersFailed(error) {
  return {
    type: FETCH_ORDERS_FAILED,
    error,
  };
}

export function addOrder(order) {
  return {
    type: ADD_ORDER,
    order,
  };
}
