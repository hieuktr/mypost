import {
  UPDATE_FCM_TOKEN,
  FETCH_PROFILE, FETCH_PROFILE_SUCCEEDED, FETCH_PROFILE_FAILED,
  UPDATE_PROFILE, UPDATE_PROFILE_SUCCEEDED, UPDATE_PROFILE_FAILED,
} from './constants';

export function updateFCMToken(fcmToken) {
  return {
    type: UPDATE_FCM_TOKEN,
    fcmToken,
  };
}

export function fetchProfile() {
  return {
    type: FETCH_PROFILE,
  };
}

export function fetchProfileSucceeded(profile, locations) {
  return {
    type: FETCH_PROFILE_SUCCEEDED,
    profile,
    locations,
  };
}

export function fetchProfileFailed(error) {
  return {
    type: FETCH_PROFILE_FAILED,
    error,
  };
}

export function updateProfile(profile, initial = false) {
  return {
    type: UPDATE_PROFILE,
    profile,
    initial,
  };
}

export function updateProfileSucceeded(profile) {
  return {
    type: UPDATE_PROFILE_SUCCEEDED,
    profile,
  };
}

export function updateProfileFailed(error) {
  return {
    type: UPDATE_PROFILE_FAILED,
    error,
  };
}