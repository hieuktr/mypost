
import { APP_API_URL } from '../../config/app';

export function updateFCMToken(token, fcmToken) {
  return fetch(`${APP_API_URL}/UpdateTokenFCM`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      token
    },
    body: JSON.stringify({
      token: fcmToken,
    }),
  });
}

export function fetchProfile(uid) {
}

export function updateProfile(uid, profile) {
}
