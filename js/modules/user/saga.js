import { put, call, takeEvery, select } from 'redux-saga/effects';
import { NavigationActions } from 'react-navigation';
import {
  fetchProfileSucceeded, fetchProfileFailed,
  updateProfileSucceeded, updateProfileFailed,
} from './actions';
import { UPDATE_FCM_TOKEN, FETCH_PROFILE, UPDATE_PROFILE } from './constants';
import { updateFCMToken, fetchProfile, updateProfile } from './apis';
import { tokenSelector } from '../auth/selectors';

function* updateFCMTokenSideEffect(action) {
  try {
    const token = yield select(tokenSelector);
    yield call(updateFCMToken, token, action.fcmToken);
  } catch (e) {
    
  }
}

function* fetchProfileSideEffect() {
  try {
    const uid = yield select(tokenSelector);
    const profile = yield call(fetchProfile, uid);
    yield put(fetchProfileSucceeded(profile));
  } catch (e) {
    yield put(fetchProfileFailed(e.message));
  }
}

function* updateProfileSideEffect(action) {
  try {
    const uid = yield select(tokenSelector);
    yield call(updateProfile, uid, action.profile);
    yield put(updateProfileSucceeded());
    const profile = yield call(fetchProfile, uid);
    yield put(fetchProfileSucceeded(profile));
    if (action.initial) {
      yield put(NavigationActions.navigate({ routeName: 'home_drawer' }));
    }
  } catch (e) {
    yield put(updateProfileFailed(e.message));
  }
}

export default function* categorySaga() {
  yield takeEvery(UPDATE_FCM_TOKEN, updateFCMTokenSideEffect);
  yield takeEvery(FETCH_PROFILE, fetchProfileSideEffect);
  yield takeEvery(UPDATE_PROFILE, updateProfileSideEffect);
}
