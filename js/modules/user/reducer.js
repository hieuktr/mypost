import { fromJS, Map } from 'immutable';
import { REHYDRATE } from 'redux-persist/constants';
import {
  FETCH_PROFILE, FETCH_PROFILE_SUCCEEDED, FETCH_PROFILE_FAILED,
  UPDATE_PROFILE, UPDATE_PROFILE_SUCCEEDED, UPDATE_PROFILE_FAILED,
} from './constants';
import { SIGN_OUT_SUCCEEDED, CHECK_AUTH_FAILED } from '../auth/constants';

const initState = fromJS({
  profile: {},
  loading: false,
  error: '',
});

export default function userReducer(state = initState, action = {}) {
  switch (action.type) {
    case FETCH_PROFILE:
      return state
        .set('loading', true)
        .set('error', '');
    case FETCH_PROFILE_SUCCEEDED:
      return state
        .set('loading', false)
        .set('profile', fromJS(Profile(action.profile)));
    case FETCH_PROFILE_FAILED:
      return state
        .set('loading', false)
        .set('error', action.error);

    case UPDATE_PROFILE:
      return state
        .set('loading', true)
        .set('error', '');
    case UPDATE_PROFILE_SUCCEEDED:
      return state
        .set('loading', false);
    case UPDATE_PROFILE_FAILED:
      return state
        .set('loading', false)
        .set('error', action.error);

    case SIGN_OUT_SUCCEEDED:
    case CHECK_AUTH_FAILED:
      return state
        .set('profile', initState.get('profile'));

    case REHYDRATE:
      const saved = action.payload && action.payload.user ? action.payload.user : state;
      const profile = Map.isMap(saved.get('profile')) ? saved.get('profile') : fromJS(saved.get('profile'));
      return state
        .set("trick", true)
        .set('profile', profile);
    default:
      return state;
  }
}
