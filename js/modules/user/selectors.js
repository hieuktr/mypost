import { createSelector } from 'reselect';

const userSelector = state => state.user;

export const profileSelector = createSelector(
  userSelector,
  userReducer => userReducer.get('profile').toJS(),
);

export const fetchProfileLoadingSelector = createSelector(
  userSelector,
  userReducer => userReducer.get('loading'),
);

export const fetchProfileErrorSelector = createSelector(
  userSelector,
  userReducer => userReducer.get('error'),
);