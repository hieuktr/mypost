import { put, call, takeEvery } from 'redux-saga/effects';
import { NavigationActions } from 'react-navigation';
import {
  signInSucceeded,
  signInFailed,
  forgotPasswordSucceeded,
  forgotPasswordFailed,
  checkAuthSucceeded,
  checkAuthFailed,
  signOutSucceeded,
  signOutFailed,
} from './actions';
import {
  SIGN_IN,
  FORGOT_PASSWORD,
  CHECK_AUTH,
  SIGN_OUT,
} from './constants';
import {
  signIn,
  forgotPassword,
  checkAuth,
  signOut,
} from './apis';
// import { fetchProfile as fetchProfileApi, createProfile } from '../user/apis';
// import { Profile } from '../user/models';
// import { fetchProfile, fetchProfileSucceeded, fetchLocations } from '../user/actions';
import { unsubscribeNotifications } from '../notification/actions';

// function* authSucceededSaga(profile) {
//   try {
//     const data = yield call(fetchProfileApi, profile.uid);
//     yield put(authSucceeded(profile.uid, profile.email));
//     if (!data.firstName && !data.lastName) {
//       yield call(createProfile, profile.uid, profile);
//       yield put(fetchProfileSucceeded(profile));
//       yield put(NavigationActions.navigate({ routeName: 'initial_profile_screen' }));
//     } else {
//       yield put(NavigationActions.navigate({ routeName: 'home_drawer' }));
//     }
//   } catch (e) {
//     yield put(authFailed(e.message));
//   }
// }

function* signInSideEffect(action) {
  try {
    const { token } = yield call(signIn, action.phoneNumber);
    // yield call(authSucceededSaga, Profile({ phoneNumber }));
    yield put(signInSucceeded(action.phoneNumber, token));
    yield put(NavigationActions.navigate({ routeName: 'home_stack' }));
  } catch (e) {
    yield put(signInFailed(e.message));
  }
}

function* forgotPasswordSideEffect(action) {
  try {
    yield call(forgotPassword, action.email);
    yield put(forgotPasswordSucceeded());
  } catch (e) {
    yield put(forgotPasswordFailed(e.message));
  }
}

function* checkAuthSideEffect() {
  try {
    yield call(checkAuth);
    yield put(checkAuthSucceeded());
    yield put(fetchProfile());
    yield put(fetchLocations());
  } catch (e) {
    yield put(unsubscribeNotifications());
    yield put(checkAuthFailed(e.message));
  }
}

function* signOutSideEffect() {
  try {
    yield call(signOut);
    // yield put(unsubscribeNotifications());
    yield put(signOutSucceeded());
    yield put(NavigationActions.navigate({ routeName: 'auth_stack' }));
  } catch (e) {
    yield put(signOutFailed(e.message));
  }
}

export default function* authSaga() {
  yield takeEvery(SIGN_IN, signInSideEffect);
  yield takeEvery(FORGOT_PASSWORD, forgotPasswordSideEffect);
  yield takeEvery(CHECK_AUTH, checkAuthSideEffect);
  yield takeEvery(SIGN_OUT, signOutSideEffect);
}
