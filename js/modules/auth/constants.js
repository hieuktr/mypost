export const SIGN_IN = 'auth/SIGN_IN';
export const SIGN_IN_SUCCEEDED = 'auth/SIGN_IN_SUCCEEDED';
export const SIGN_IN_FAILED = 'auth/SIGN_IN_FAILED';

export const FORGOT_PASSWORD = 'auth/FORGOT_PASSWORD';
export const FORGOT_PASSWORD_SUCCEEDED = 'auth/FORGOT_PASSWORD_SUCCEEDED';
export const FORGOT_PASSWORD_FAILED = 'auth/FORGOT_PASSWORD_FAILED';

export const CHECK_AUTH = 'auth/CHECK_AUTH';
export const CHECK_AUTH_SUCCEEDED = 'auth/CHECK_AUTH_SUCCEEDED';
export const CHECK_AUTH_FAILED = 'auth/CHECK_AUTH_FAILED';

export const SIGN_OUT = 'auth/SIGN_OUT';
export const SIGN_OUT_SUCCEEDED = 'auth/SIGN_OUT_SUCCEEDED';
export const SIGN_OUT_FAILED = 'auth/SIGN_OUT_FAILED';
