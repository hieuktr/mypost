import { fromJS } from 'immutable';
import { REHYDRATE } from 'redux-persist/constants';
import {
  SIGN_IN,
  SIGN_IN_SUCCEEDED,
  SIGN_IN_FAILED,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCEEDED,
  FORGOT_PASSWORD_FAILED,
  CHECK_AUTH,
  CHECK_AUTH_SUCCEEDED,
  CHECK_AUTH_FAILED,
  SIGN_OUT,
  SIGN_OUT_SUCCEEDED,
  SIGN_OUT_FAILED,
} from './constants';

const initState = fromJS({
  phoneNumber: '',
  token: '',
  signInLoading: false,
  signInError: '',
  forgotPasswordLoading: false,
  forgotPasswordError: '',
  checkAuthLoading: false,
  checkAuthError: '',
  signOutLoading: false,
  signOutError: ''
});

export default function authReducer(state = initState, action = {}) {
  switch (action.type) {
    case SIGN_IN:
      return state
        .set('signInLoading', true)
        .set('signInError', '');
    case SIGN_IN_SUCCEEDED:
      return state
        .set('signInLoading', false)
        .set('phoneNumber', action.phoneNumber)
        .set('token', action.token);
    case SIGN_IN_FAILED:
      return state
        .set('signInLoading', false)
        .set('signInError', action.error);

    case FORGOT_PASSWORD:
      return state
        .set('forgotPasswordLoading', true)
        .set('forgotPasswordError', '');
    case FORGOT_PASSWORD_SUCCEEDED:
      return state
        .set('forgotPasswordLoading', false);
    case FORGOT_PASSWORD_FAILED:
      return state
        .set('forgotPasswordLoading', false)
        .set('forgotPasswordError', action.error);

    case CHECK_AUTH:
      return state
        .set('checkAuthLoading', true)
        .set('checkAuthError', '');
    case CHECK_AUTH_SUCCEEDED:
      return state
        .set('checkAuthLoading', false);
    case CHECK_AUTH_FAILED:
      return state
        .set('uid', '')
        .set('email', '')
        .set('checkAuthLoading', false)
        .set('checkAuthError', action.error);

    case SIGN_OUT:
      return state
        .set('signOutLoading', true)
        .set('signOutError', '');
    case SIGN_OUT_SUCCEEDED:
      return state
        .set('phoneNumber', '')
        .set('token', '')
        .set('signOutLoading', false);
    case SIGN_OUT_FAILED:
      return state
        .set('signOutLoading', false)
        .set('signOutError', action.error);

    case REHYDRATE:
      const saved = action.payload && action.payload.auth ? action.payload.auth : state;
      return state
        .set("trick", true)
        .set('phoneNumber', saved.get('phoneNumber'))
        .set('token', saved.get('token'));
    default:
      return state;
  }
}
