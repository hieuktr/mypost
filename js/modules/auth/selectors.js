import { createSelector } from 'reselect';

const authSelector = state => state.auth;

export const phoneNumberSelector = createSelector(
  authSelector,
  authReducer => authReducer.get('phoneNumber'),
);

export const tokenSelector = createSelector(
  authSelector,
  authReducer => authReducer.get('token'),
);

export const signInLoadingSelector = createSelector(
  authSelector,
  authReducer => authReducer.get('signInLoading'),
);

export const signInErrorSelector = createSelector(
  authSelector,
  authReducer => authReducer.get('signInError'),
);

export const forgotPasswordLoadingSelector = createSelector(
  authSelector,
  authReducer => authReducer.get('forgotPasswordLoading'),
);

export const forgotPasswordErrorSelector = createSelector(
  authSelector,
  authReducer => authReducer.get('forgotPasswordError'),
);

export const checkAuthLoadingSelector = createSelector(
  authSelector,
  authReducer => authReducer.get('checkAuthLoading'),
);

export const checkAuthErrorSelector = createSelector(
  authSelector,
  authReducer => authReducer.get('checkAuthError'),
);
