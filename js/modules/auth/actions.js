import {
  SIGN_IN,
  SIGN_IN_SUCCEEDED,
  SIGN_IN_FAILED,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCEEDED,
  FORGOT_PASSWORD_FAILED,
  CHECK_AUTH,
  CHECK_AUTH_SUCCEEDED,
  CHECK_AUTH_FAILED,
  SIGN_OUT,
  SIGN_OUT_SUCCEEDED,
  SIGN_OUT_FAILED,
} from './constants';

export function signIn(phoneNumber) {
  return {
    type: SIGN_IN,
    phoneNumber,
  };
}

export function signInSucceeded(phoneNumber, token) {
  return {
    type: SIGN_IN_SUCCEEDED,
    phoneNumber,
    token,
  };
}

export function signInFailed(error) {
  return {
    type: SIGN_IN_FAILED,
    error,
  };
}

export function forgotPassword(email) {
  return {
    type: FORGOT_PASSWORD,
    email,
  };
}

export function forgotPasswordSucceeded() {
  return {
    type: FORGOT_PASSWORD_SUCCEEDED,
  };
}

export function forgotPasswordFailed(error) {
  return {
    type: FORGOT_PASSWORD_FAILED,
    error,
  };
}

export function checkAuth() {
  return {
    type: CHECK_AUTH,
  };
}

export function checkAuthSucceeded() {
  return {
    type: CHECK_AUTH_SUCCEEDED,
  };
}

export function checkAuthFailed(error) {
  return {
    type: CHECK_AUTH_FAILED,
    error,
  };
}

export function signOut() {
  return {
    type: SIGN_OUT,
  };
}

export function signOutSucceeded() {
  return {
    type: SIGN_OUT_SUCCEEDED,
  };
}

export function signOutFailed(error) {
  return {
    type: SIGN_OUT_FAILED,
    error,
  };
}
