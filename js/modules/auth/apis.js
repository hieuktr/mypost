
import { APP_API_URL } from '../../config/app';

export function signIn(phoneNumber) {
  return fetch(`${APP_API_URL}/signIn`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    body: JSON.stringify({
      phone: phoneNumber,
    }),
  })
    .then(response => response.json())
    .then(json => {
      if (json.status) {
        return {
          token: json.token
        }
      } else {
        throw new Error(json.msg)
      }
    });
}

export function signOut() {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(), 100);
  });
}
