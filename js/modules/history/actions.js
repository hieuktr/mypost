import {
  FETCH_HISTORY,
  FETCH_HISTORY_SUCCEEDED,
  FETCH_HISTORY_FAILED,
} from './constants';

export function fetchHistory() {
  return {
    type: FETCH_HISTORY,
  };
}

export function fetchHistorySucceeded(history) {
  return {
    type: FETCH_HISTORY_SUCCEEDED,
    history,
  };
}

export function fetchHistoryFailed(error) {
  return {
    type: FETCH_HISTORY_FAILED,
    error,
  };
}
