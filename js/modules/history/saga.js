import { call, takeEvery, select, put } from 'redux-saga/effects';
import { fetchHistorySucceeded, fetchHistoryFailed } from './actions';
import { FETCH_HISTORY } from './constants';
import { fetchHistory } from './apis';
import { tokenSelector } from '../auth/selectors';

function* fetchHistorySideEffect() {
  try {
    const token = yield select(tokenSelector);
    const { history } = yield call(fetchHistory, token);
    yield put(fetchHistorySucceeded(history));
  } catch (e) {
    yield put(fetchHistoryFailed(e.message));
  }
}

export default function* notificationSaga() {
  yield takeEvery(FETCH_HISTORY, fetchHistorySideEffect);
}
