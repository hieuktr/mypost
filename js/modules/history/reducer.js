import { fromJS } from 'immutable';
import {
  FETCH_HISTORY,
  FETCH_HISTORY_SUCCEEDED,
  FETCH_HISTORY_FAILED,
} from './constants';

const initState = fromJS({
  history: {},
  loading: false,
  error: '',
});

export default function historyReducer(state = initState, action = {}) {
  switch (action.type) {
    case FETCH_HISTORY:
      return state
        .set('loading', true)
        .set('error', '');
    case FETCH_HISTORY_SUCCEEDED:
      return state
        .set('loading', false)
        .set('history', fromJS(action.history));
    case FETCH_HISTORY_FAILED:
      return state
        .set('loading', false)
        .set('error', action.error);
    default:
      return state;
  }
}
