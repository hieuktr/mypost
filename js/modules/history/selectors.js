import { createSelector } from 'reselect';

const historyReducerSelector = state => state.history;

export const historySelector = createSelector(
  historyReducerSelector,
  historyReducer => historyReducer.get('history').toJS(),
);

export const fetchHistoryLoadingSelector = createSelector(
  historyReducerSelector,
  historyReducer => historyReducer.get('loading'),
);

export const fetchNotificationsErrorSelector = createSelector(
  historyReducerSelector,
  historyReducer => historyReducer.get('error'),
);
