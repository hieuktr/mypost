
import { APP_API_URL } from '../../config/app';

export function fetchHistory(token) {
  return fetch(`${APP_API_URL}/GetLichSu`, {
    method: "GET",
    headers: {
      token,
    },
  })
    .then(response => response.json())
    .then(json => {
      console.log(json)
      if (json.status) {
        return {
          history: {
            taichinh: json.taichinh || {},
            thongke: json.thongke || {},
          },
        }
      } else {
        throw new Error(json.msg)
      }
    });
}
