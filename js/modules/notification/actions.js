import {
  FETCH_NOTIFICATIONS,
  UNSUBSCRIBE_NOTIFICATIONS,
  FETCH_NOTIFICATIONS_SUCCEEDED,
  FETCH_NOTIFICATIONS_FAILED,
  SEEN_ALL_NOTIFICATIONS,

  UPDATE_NOTIFICATION,
  REMOVE_NOTIFICATION,
  REMOVE_NOTIFICATIONS,
  REMOVE_ALL_NOTIFICATIONS,
} from './constants';

export function fetchNotifications() {
  return {
    type: FETCH_NOTIFICATIONS,
  };
}

export function unsubscribeNotifications() {
  return {
    type: UNSUBSCRIBE_NOTIFICATIONS,
  };
}

export function fetchNotificationsSucceeded(notifications) {
  return {
    type: FETCH_NOTIFICATIONS_SUCCEEDED,
    notifications,
  };
}

export function fetchNotificationsFailed(error) {
  return {
    type: FETCH_NOTIFICATIONS_FAILED,
    error,
  };
}

export function seenAllNotifications() {
  return {
    type: SEEN_ALL_NOTIFICATIONS,
  };
}

export function updateNotification(notificationId) {
  return {
    type: UPDATE_NOTIFICATION,
    notificationId,
  };
}


export function removeNotification(notificationId) {
  return {
    type: REMOVE_NOTIFICATION,
    notificationId,
  };
}

export function removeNotifications(notificationIds) {
  return {
    type: REMOVE_NOTIFICATIONS,
    notificationIds,
  };
}

export function removeAllNotifications() {
  return {
    type: REMOVE_ALL_NOTIFICATIONS,
  };
}
