import { put, call, takeEvery, select } from 'redux-saga/effects';
import { fetchNotificationsSucceeded, fetchNotificationsFailed } from './actions';
import {
  FETCH_NOTIFICATIONS,
  UPDATE_NOTIFICATION,
  REMOVE_NOTIFICATION,
  REMOVE_NOTIFICATIONS,
  REMOVE_ALL_NOTIFICATIONS,
} from './constants';
import {
  fetchNotifications,
  updateNotification,
  removeNotification,
  removeNotifications,
  removeAllNotifications,
} from './apis';
import { tokenSelector } from '../auth/selectors';

function* fetchNotificationsSideEffect() {
  try {
    const token = yield select(tokenSelector);
    const { notifications } = yield call(fetchNotifications, token);
    yield put(fetchNotificationsSucceeded(notifications));
  } catch (e) {
    yield put(fetchNotificationsFailed(e.message));
  }
}

function* updateNotificationSideEffect(action) {
  try {
    const uid = yield select(uidSelector);
    yield call(updateNotification, uid, action.notificationId);
  } catch (e) {

  }
}

function* removeNotificationSideEffect(action) {
  try {
    const uid = yield select(uidSelector);
    yield call(removeNotification, uid, action.notificationId);
  } catch (e) {

  }
}

function* removeNotificationsSideEffect(action) {
  try {
    const uid = yield select(uidSelector);
    yield call(removeNotifications, uid, action.notificationIds);
  } catch (e) {

  }
}

function* removeAllNotificationsSideEffect() {
  try {
    const uid = yield select(uidSelector);
    yield call(removeAllNotifications, uid);
  } catch (e) {

  }
}

export default function* notificationSaga() {
  yield takeEvery(FETCH_NOTIFICATIONS, fetchNotificationsSideEffect);
  // yield takeEvery(UPDATE_NOTIFICATION, updateNotificationSideEffect);
  // yield takeEvery(REMOVE_NOTIFICATION, removeNotificationSideEffect);
  // yield takeEvery(REMOVE_NOTIFICATIONS, removeNotificationsSideEffect);
  // yield takeEvery(REMOVE_ALL_NOTIFICATIONS, removeAllNotificationsSideEffect);
}
