
import { APP_API_URL } from '../../config/app';

export function fetchNotifications(token) {
  return fetch(`${APP_API_URL}/ListThongBao`, {
    method: "GET",
    headers: {
      token,
    },
  })
    .then(response => response.json())
    .then(json => {
      if (json.status) {
        return {
          notifications: (json.thongbaos || []).map(notification => ({ ...notification, seen: false })),
        }
      } else {
        throw new Error(json.msg)
      }
    });
}

export function updateNotification(uid, notificationId) {
}

export function removeNotification(uid, notificationId) {
}

export function removeNotifications(uid, notificationIds) {
}

export function removeAllNotifications(uid) {
}

