import { fromJS } from 'immutable';
import {
  FETCH_NOTIFICATIONS,
  FETCH_NOTIFICATIONS_SUCCEEDED,
  FETCH_NOTIFICATIONS_FAILED,
  SEEN_ALL_NOTIFICATIONS,
} from './constants';

const initState = fromJS({
  notifications: [],
  loading: false,
  error: '',
});

export default function notificationReducer(state = initState, action = {}) {
  switch (action.type) {
    case FETCH_NOTIFICATIONS:
      return state
        .set('loading', true)
        .set('error', '');
    case FETCH_NOTIFICATIONS_SUCCEEDED:
      return state
        .set('loading', false)
        .set('notifications', fromJS(action.notifications));
    case FETCH_NOTIFICATIONS_FAILED:
      return state
        .set('loading', false)
        .set('error', action.error);
    case SEEN_ALL_NOTIFICATIONS:
      return state
        .set('notifications', state.get('notifications').map(notification => notification.set('seen', true)));
    default:
      return state;
  }
}
