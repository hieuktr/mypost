import { createSelector } from 'reselect';

const notificationSelector = state => state.notification;

export const notificationsSelector = createSelector(
  notificationSelector,
  notificationReducer => notificationReducer.get('notifications').toJS(),
);

export const notificationCountSelector = createSelector(
  notificationSelector,
  notificationReducer => notificationReducer.get('notifications').filter(notification => !notification.get('seen')).count(),
);

export const fetchNotificationsLoadingSelector = createSelector(
  notificationSelector,
  notificationReducer => notificationReducer.get('loading'),
);

export const fetchNotificationsErrorSelector = createSelector(
  notificationSelector,
  notificationReducer => notificationReducer.get('error'),
);
