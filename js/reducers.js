import { combineReducers } from 'redux';
import { navigationReducer } from './navigators/redux';
import authReducer from './modules/auth/reducer';
import userReducer from './modules/user/reducer';
import orderReducer from './modules/order/reducer';
import historyReducer from './modules/history/reducer';
import notificationReducer from './modules/notification/reducer';
import messageReducer from './modules/message/reducer';

const rootReducer = combineReducers({
  nav: navigationReducer,
  auth: authReducer,
  user: userReducer,
  order: orderReducer,
  history: historyReducer,
  notification: notificationReducer,
  message: messageReducer,
});

export default rootReducer;
