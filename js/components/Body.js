import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ScrollView = styled.ScrollView.attrs({
  contentContainerStyle: props => {
    if (props.p) {
      return {
        padding: props.p,
      }
    }
    if (props.full) {
      return {
        flex: 1,
      }
    }
  }
})`
  ${({ brand, theme }) => brand && `
    background-color: ${theme.color.primary};
  `}
`;

const View = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  padding: 15px;
  background-color: ${({ theme }) => theme.color.primary};
`;

const Body = ({ style, children, brand, p, full, ...rest }) => (
  brand ? (
    <View style={style}>
      {children}
    </View>
  ) : (
    <ScrollView style={style} brand={brand} p={p} full={full} {...rest}>
      {children}
    </ScrollView>
  )
);

Body.propTypes = {
  style: PropTypes.any,
  children: PropTypes.any,
  brand: PropTypes.bool,
  full: PropTypes.bool,
  p: PropTypes.any,
};

export default Body;
