import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Text from './Text';

const View = styled.View`
  background-color: ${({ theme }) => theme.color.light};
  padding: 10px;
  box-shadow: 2px 2px 2px rgba(0,0,0,0.5);
  elevation: 2;
  margin-bottom: 15px;
  ${({ m }) => m && `margin: ${m};`}
`;

const FormCard = ({ style, title, children, ...rest }) => {
  return (
    <View style={style} {...rest}>
      {title && (
        typeof title === "string" ?
        <Text bold m="0 0 10px">{title}</Text> :
        title
      )}
      {children}
    </View>
  );
}

FormCard.propTypes = {
  style: PropTypes.any,
  title: PropTypes.any,
  children: PropTypes.any,
};

export default FormCard;
