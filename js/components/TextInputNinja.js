import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Icon } from 'react-native-elements';
import Text from './Text';
import theme from '../config/theme';

const View = styled.View`
  margin-bottom: 10px;
  align-items: flex-end;
  ${({ m }) => m && `margin: ${m};`}
  ${({ center }) => center && `text-align: center;`}
`;

const Row = styled.View`
  flex-direction: row;
  align-items: center;
  background-color: ${({ theme }) => theme.color.light};
  border: 1px solid ${({ theme, error }) => error ? theme.color.error : theme.color.primaryLight};
  border-radius: ${({ round, theme }) => round ? 25 : theme.other.radius}px;
  padding: 5px 15px;
  ${({ noborder }) => noborder && `border-color: transparent;`}
`;

const TextInput = styled.TextInput.attrs({
  placeholderTextColor: ({ theme }) => theme.color.gray,
  underlineColorAndroid: "transparent",
})`
  color: ${({ theme }) => theme.color.dark};
  font-family: ${({ theme }) => theme.font.primary};
  font-size: ${({ theme }) => theme.typo.p.size};
  flex: 1;
`;

class TextInputNinja extends Component {
  render() {
    const { style, m, icon, errors, onChange, value, round, noborder, ...rest } = this.props;
    return (
      <View m={m} style={style}>
        <Row error={errors && errors.length > 0} round={round} noborder={noborder}>
          {icon && 
            <Icon
              name={icon.name}
              type={icon.type}
              size={icon.size || 19}
              color={theme.color.gray}
              containerStyle={{ marginRight: 10, width: 25 }}
            />}
          <TextInput value={value ? value.toString() : ""} onChangeText={onChange} {...rest} />
        </Row>
        {errors && <Text error small m="5px">{errors[0]}</Text>}
      </View>
    );
  }
}

TextInputNinja.propTypes = {
  style: PropTypes.any,
  icon: PropTypes.object,
  m: PropTypes.string,
  errors: PropTypes.any,
  onChange: PropTypes.func,
  round: PropTypes.bool,
  noborder: PropTypes.bool,
};

export default TextInputNinja;
