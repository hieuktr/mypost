import React, { PureComponent } from 'react';
import styled from 'styled-components';
import BrandText from './BrandText';

const View = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.color.primary};
`;

class Splash extends PureComponent {
  render() {
    const { children, style, onPress } = this.props;
    return (
      <View>
        <BrandText />
      </View>
    );
  }
}

export default Splash;
