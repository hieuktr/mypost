import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Modal as RNModal, TouchableOpacity, Dimensions } from 'react-native';
import { BlurView } from 'expo';
import Text from './Text';

const styles = StyleSheet.create({
  modal: {
    marginTop: -24,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    height: Dimensions.get('window').height,
  },
  container: {
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingTop: 10,
  },
  flex: {
    flex: 1,
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
  },
  back: {
    color: '#0D55D0',
  },
});

class Modal extends Component {
  render() {
    const { visible, onRequestClose, title, onBackPress, children } = this.props;

    return (
      <RNModal
        visible={visible}
        onRequestClose={onRequestClose}
        transparent={true}
        animationType="none">
        <View style={styles.modal}>
          <TouchableOpacity onPress={onRequestClose} style={styles.flex} activeOpacity={1}>
            <BlurView tint="dark" intensity={50} style={styles.flex} />
          </TouchableOpacity>
          <View style={styles.container}>
            <View style={styles.title}>
              <Text h4 bold>{title}</Text>
              {onBackPress ? (
                <TouchableOpacity onPress={onBackPress}>
                  <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
              ) : null}
            </View>
            {children}
          </View>
        </View>
      </RNModal>
    );
  }
}

Modal.propTypes = {
  visible: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  onBackPress: PropTypes.func,
  children: PropTypes.any.isRequired,
}

export default Modal;
