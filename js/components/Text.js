import styled from 'styled-components';

const Text = styled.Text`
  color: ${({ theme, primary, primaryLight, secondary, light, gray, error }) => {
    if (primary) return theme.color.primary;
    if (primaryLight) return theme.color.primaryLight;
    if (secondary) return theme.color.secondary;
    if (light) return theme.color.light;
    if (gray) return theme.color.gray;
    if (error) return theme.color.error;
    return theme.color.dark;
  }};
  font-family: ${({ theme }) => theme.font.primary};
  font-size: ${({ theme }) => theme.typo.p.size};
  line-height:  ${({ theme }) => theme.typo.p.lineHeight};
  ${({ m }) => m && `margin: ${m};`}
  ${({ flex }) => flex && `flex: 1;`}
  ${({ center }) => center && `text-align: center;`}
  ${({ light, theme }) => light && `font-family: ${theme.font.primaryLight};`}
  ${({ medium, theme }) => medium && `font-family: ${theme.font.primaryMedium};`}
  ${({ bold, theme }) => bold && `font-family: ${theme.font.primaryBold};`}
  ${({ h1 }) => h1 && `
    font-size: ${theme.typo.h1.size};
    line-height: ${theme.typo.h1.lineHeight};
  `}
  ${({ h2 }) => h2 && `
    font-size: ${theme.typo.h2.size};
    line-height: ${theme.typo.h2.lineHeight};
  `}
  ${({ h3 }) => h3 && `
    font-size: ${theme.typo.h3.size};
    line-height: ${theme.typo.h3.lineHeight};
  `}
  ${({ h4 }) => h4 && `
    font-size: ${theme.typo.h4.size};
    line-height: ${theme.typo.h4.lineHeight};
  `}
  ${({ small }) => small && `
    font-size: ${theme.typo.small.size};
    line-height: ${theme.typo.small.lineHeight};
  `}
`;

export default Text;
