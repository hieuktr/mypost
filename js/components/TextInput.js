import styled from 'styled-components';

const TextInput = styled.TextInput.attrs({
  placeholderTextColor: ({ theme }) => theme.color.gray,
  underlineColorAndroid: "transparent",
})`
  color: ${({ theme }) => theme.color.dark};
  font-family: ${({ theme }) => theme.font.primary};
  font-size: ${({ theme }) => theme.typo.h4.size};
  width: 100%;
  height: 45px;
  background-color: ${({ theme }) => theme.color.light};
  border: 1px solid ${({ theme }) => theme.color.light};
  border-radius: ${({ round, theme }) => round ? 25 : theme.other.radius}px;
  padding-horizontal: 20px;
  ${({ m }) => m && `margin: ${m};`}
  ${({ center }) => center && `text-align: center;`}
`;

export default TextInput;
