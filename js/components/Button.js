import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Text from './Text';

const TouchableOpacity = styled.TouchableOpacity`
  height: 45px;
  padding-horizontal: 30px;
  border-radius: ${({ round, theme }) => round ? 25 : theme.other.radius}px;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme, primary, secondary, light, transparent }) => {
    if (primary) return theme.color.primary;
    if (secondary) return theme.color.secondary;
    if (light) return theme.color.light;
    if (transparent) return 'transparent';
    return theme.color.secondary;
  }};
  ${({ m }) => m && `margin: ${m};`}
  ${({ block }) => block && `width: 100%;`}
  ${({ disabled }) => disabled && `opacity: 0.5;`}
`;

const Button = ({ style, title, onPress, light, transparent, ...rest  }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={style}
      onPress={onPress}
      light={light}
      transparent={transparent}
      {...rest}>
      <Text light={!light && !transparent} dark={light && transparent} h4>{title}</Text>
    </TouchableOpacity>
  );
}

Button.propTypes = {
  style: PropTypes.any,
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
};

export default Button;
