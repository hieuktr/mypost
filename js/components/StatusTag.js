import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Text from './Text';
import { STATUSES } from '../config/app';

const View = styled.View`
  background-color: ${({ statusCode }) => STATUSES[statusCode].color};
  padding: 7px 13px;
  border-radius: 20px;
  align-self: flex-start;
  ${({ m }) => m && `margin: ${m};`}
`;

const StatusTag = ({ style, statusCode, ...rest }) => {
  return (
    <View style={style} statusCode={statusCode} {...rest}>
      <Text h4>{STATUSES[statusCode].name}</Text>
    </View>
  );
}

StatusTag.propTypes = {
  style: PropTypes.any,
  statusCode: PropTypes.string.isRequired,
};

export default StatusTag;
