import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Text from './Text';

const Row = styled.View`
  flex-direction: row;
  background-color: ${({ theme }) => theme.color.light};
  padding: 5px 0;
  box-shadow: 2px 2px 2px rgba(0,0,0,0.5);
  elevation: 2;
  margin-bottom: 15px;
`;
const Step = styled.View`
  flex: 1;
  align-items: center;
`;

const Round = styled.Text`
  color: ${({ theme }) => theme.color.light};
  border: 1px solid white;
  border-color: transparent;
  background-color: ${({ theme }) => theme.color.primaryLight};
  border-radius: 25px;
  width: 20px;
  line-height: 18px;
  text-align: center;
  font-size: 12px;
  ${({ active, theme }) => active && `background-color: ${theme.color.secondary};`}
`;

const Progress = ({ style, steps, currentStep }) => {
  return (
    <Row style={style}>
      {steps.map((step, index) => {
        const active = currentStep >= parseInt(index, 10);
        return (
          <Step key={index} >
            <Round active={active}>{parseInt(index, 10) + 1}</Round>
            <Text bold primaryLight={!active} secondary={active}>{step}</Text>
          </Step>
        )
      })}
    </Row>
  );
}

Progress.propTypes = {
  style: PropTypes.any,
  steps: PropTypes.array.isRequired,
  currentStep: PropTypes.number,
};

export default Progress;
