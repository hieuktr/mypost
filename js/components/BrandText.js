import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Text = styled.Text`
  color: ${({ theme, light }) => {
    if (light) return theme.color.light;
    return theme.color.secondary;
  }};
  font-size: 50px;
  font-family: ${({ theme }) => theme.font.brand};
  align-self: ${props => props.center ? 'center' : 'auto'};
  /* text-shadow: 1px 1px 30px rgba(0,0,0,0.15); */
`;

const BrandText = ({ style, center }) => (
  <Text style={style} center={center}>
    I<Text light>POST</Text>
  </Text>
);

BrandText.propTypes = {
  style: PropTypes.any,
  center: PropTypes.bool,
}

export default BrandText;
