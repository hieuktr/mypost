import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Icon, Badge } from 'react-native-elements';

import { THEME_COLOR } from '../../config/style';
import { cartCountSelector } from '../../modules/cart/selectors';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingLeft: 15,
    paddingRight: 20,
  },
  badge: {
    position: 'absolute',
    top: -6,
    left: 10,
    backgroundColor: 'white',
    paddingHorizontal: 4,
    paddingTop: 0,
    paddingBottom: 0,
    borderWidth: 2,
    borderColor: THEME_COLOR,
    zIndex: 1,
  },
  badgeText: {
    color: THEME_COLOR,
    lineHeight: 12,
    fontSize: 10,
    fontWeight: 'bold',
  },
});

class CartIcon extends Component {
  render() {
    const { navigate, cartCount } = this.props;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => navigate('cart_stack')}
      >
        {cartCount > 0 ? (
          <Badge
            value={cartCount}
            containerStyle={styles.badge}
            textStyle={styles.badgeText}
          />
        ) : null}
        <Icon
          type="ionicon"
          name="md-cart"
          color="white"
          size={24}
        />
      </TouchableOpacity>
    );
  }
}

CartIcon.propTypes = {
  cartCount: PropTypes.number.isRequired,
  navigate: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  cartCount: cartCountSelector(state),
});

const mapDispatchToProps = dispatch => ({
  navigate: routeName => dispatch(NavigationActions.navigate({ routeName })),
});

export default connect(mapStateToProps, mapDispatchToProps)(CartIcon);
