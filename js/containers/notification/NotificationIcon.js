import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import { Icon, Badge } from 'react-native-elements';

import { notificationCountSelector } from '../../modules/notification/selectors';
import theme from '../../config/theme';

const styles = StyleSheet.create({
  badge: {
    position: 'absolute',
    top: -3,
    left: 10,
    backgroundColor: theme.color.secondary,
    paddingHorizontal: 4,
    paddingTop: 0,
    paddingBottom: 0,
    borderWidth: 1,
    borderColor: theme.color.primary,
    zIndex: 1,
  },
  badgeText: {
    color: theme.color.primary,
    lineHeight: 12,
    fontSize: 10,
    fontWeight: 'bold',
  },
});

class NotificationIcon extends Component {
  render() {
    const { notificationCount, type, name, color } = this.props;
    return (
      <View>
        {notificationCount > 0 ? (
          <Badge
            value={notificationCount}
            containerStyle={styles.badge}
            textStyle={styles.badgeText}
          />
        ) : null}
        <Icon
          type={type}
          name={name}
          color={color}
        />
      </View>
    );
  }
}

NotificationIcon.propTypes = {
  notificationCount: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  notificationCount: notificationCountSelector(state),
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationIcon);
