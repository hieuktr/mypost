import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, FlatList, RefreshControl } from 'react-native';
import styled from 'styled-components';
import TimeAgo from 'react-native-timeago';

import { Body, Text } from '../../components';

import { fetchNotifications, seenAllNotifications } from '../../modules/notification/actions';
import {
  notificationsSelector,
  fetchNotificationsLoadingSelector,
} from '../../modules/notification/selectors';

const styles = StyleSheet.create({
  list: {
    padding: 15,
  },
});

const ListItem = styled.View`
  background-color: white;
  padding: 15px;
  border: 0 solid ${({ theme }) => theme.color.lightgray};
  border-bottom-width: 1px;
  margin-bottom: 10px;
  border-radius: 5px;
`;

class Notifications extends Component {

  componentDidMount() {
    // this.fetchNotifications();
    // this.props.seenAllNotifications();
  }

  fetchNotifications() {
    this.props.fetchNotifications();
  }

  render() {
    const { navigation, notifications, loading } = this.props;

    if (!notifications || notifications.length === 0) {
      return (
        <Body
          p={15}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={() => this.fetchNotifications()}
            />
          }
        >
          <Text h4 center m="15px 0 50px">Không có thông báo nào.</Text>
        </Body>
      );
    }

    return (
      <FlatList
        contentContainerStyle={styles.list}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={() => this.fetchNotifications()}
          />
        }
        data={notifications}
        keyExtractor={(item, idx) => idx+""}
        renderItem={({ item: notification }) => (
          <ListItem>
            <Text bold h4 m="0 0 4px">{notification.sender}</Text>
            <Text gray small m="0 0 10px">
              <TimeAgo time={notification.time} />
            </Text>
            <Text h4>{notification.content}</Text>
          </ListItem>
        )}
      />
    );
  }
}

Notifications.navigationOptions = ({ navigation }) => ({
  headerTitle: "Thông báo",
});

Notifications.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  notifications: notificationsSelector(state),
  loading: fetchNotificationsLoadingSelector(state),
});

const mapDispatchToProps = dispatch => ({
  fetchNotifications: () => dispatch(fetchNotifications()),
  seenAllNotifications: () => dispatch(seenAllNotifications()),
});


export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
