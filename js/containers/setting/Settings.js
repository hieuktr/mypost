import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View } from 'react-native';
import styled from 'styled-components';
import { Icon } from 'react-native-elements';

import { Body, Text } from '../../components';
import { signOut } from '../../modules/auth/actions';
import theme from '../../config/theme';

const ListItem = styled.TouchableOpacity`
  background-color: white;
  padding: 15px;
  border: 0 solid ${({ theme }) => theme.color.lightgray};
  border-bottom-width: 1px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

class Settings extends Component {

  render() {
    const { navigation: { navigate }, signOut } = this.props;
    return (
      <Body>
        <ListItem onPress={() => navigate("auth_stack")}>
          <Text h4>Thông tin tài khoản</Text>
          <Icon type="entypo" name="chevron-small-right" color={theme.color.dark} />
        </ListItem>
        <ListItem onPress={() => navigate("auth_stack")}>
          <Text h4>Giới thiệu</Text>
          <Icon type="entypo" name="chevron-small-right" color={theme.color.dark} />
        </ListItem>
        <ListItem onPress={() => navigate("auth_stack")}>
          <Text h4>Cài đặt</Text>
          <Icon type="entypo" name="chevron-small-right" color={theme.color.dark} />
        </ListItem>
        <ListItem onPress={signOut}>
          <Text h4>Đăng xuất</Text>
          <Icon type="entypo" name="chevron-small-right" color={theme.color.dark} />
        </ListItem>
      </Body>
    );
  }
}

Settings.navigationOptions = ({ navigation }) => ({
  headerTitle: "Danh mục",
});

Settings.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut()),
});


export default connect(mapStateToProps, mapDispatchToProps)(Settings);
