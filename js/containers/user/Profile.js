import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, ScrollView, Alert } from 'react-native';

import AddLocationModal from './AddLocationModal';
import ProfileInfo from './components/ProfileInfo';
import LocationListItem from './components/LocationListItem';

import { LayoutWithAction, Button, Text } from '../../components';
import { profileSelector, fullNameSelector, locationsSelector } from '../../modules/user/selectors';
import { removeLocation } from '../../modules/user/actions';
import { signOut } from '../../modules/auth/actions';

const styles = StyleSheet.create({
  list: {
    padding: 15,
  },
  locationText: {
    margin: 15,
    marginBottom: 0,
  },
});

class Profile extends Component {
  state = {
    visible: false,
  };
  
  signOut() {
    Alert.alert(
      'Are you sure?',
      '',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => {
          this.props.signOut();
          this.props.navigation.navigate('categories_screen');
        }},
      ],
    );
  }

  render() {
    const { profile, fullName, locations, removeLocation, navigation } = this.props;
    const { visible } = this.state;

    return (
      <LayoutWithAction
        action={
          <Button title="Sign Out" onPress={() => this.signOut()} />
        }
      >
        <ProfileInfo
          {...profile}
          fullName={fullName}
          onEditPress={() => navigation.navigate('edit_profile_screen')}
        />
        <Text style={styles.locationText} bold>Your locations</Text>
        <ScrollView contentContainerStyle={styles.list}>
          {locations.map(location => (
            <LocationListItem
              key={location.id}
              owner={location.owner}
              address={location.address}
              onPress={() => null}
              onRemovePress={() => removeLocation(location.id)}
            />
          ))}
        </ScrollView>
        <Button title="Add Location" onPress={() => this.setState({ visible: true })}/>
        {visible && 
          <AddLocationModal
            visible={true}
            onRequestClose={() => this.setState({ visible: false })}
          />}
      </LayoutWithAction>
    );
  }
}

Profile.navigationOptions = {
  headerTitle: 'Profile',
};

Profile.propTypes = {
  profile: PropTypes.object.isRequired,
  fullName: PropTypes.string.isRequired,
  locations: PropTypes.array.isRequired,
  signOut: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  profile: profileSelector(state),
  fullName: fullNameSelector(state),
  locations: locationsSelector(state),
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut()),
  removeLocation: (locationId) => dispatch(removeLocation(locationId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
