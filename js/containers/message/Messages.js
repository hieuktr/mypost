import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, FlatList, RefreshControl, View } from 'react-native';
import styled from 'styled-components';
import TimeAgo from 'react-native-timeago';

import { Body, Text, TextInput, Button } from '../../components';

import { fetchMessages, postMessage } from '../../modules/message/actions';
import {
  messagesSelector,
  fetchMessagesLoadingSelector,
  postMessageLoadingSelector,
} from '../../modules/message/selectors';
import { phoneNumberSelector } from '../../modules/auth/selectors';

const styles = StyleSheet.create({
  list: {
    padding: 15,
  },
});

const ListItem = styled.View`
  background-color: white;
  padding: 15px;
  margin-bottom: 15px;
  border-radius: 5px;
  align-self: flex-end;
  align-items: flex-end;
  max-width: 80%;
`;

const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin: 15px;
`;

class Messages extends Component {

  state = {
    content: ''
  }

  componentDidMount() {
    this.fetchMessages();
  }

  fetchMessages() {
    this.props.fetchMessages(this.props.navigation.state.params.itemCode);
  }

  postMessage() {
    this.props.postMessage({
      itemCode: this.props.navigation.state.params.itemCode,
      sender: this.props.phoneNumber,
      content: this.state.content,
      time: new Date().toISOString(),
    });
    this.setState({ content: '' });
  }

  render() {
    const { messages, loading, postMessageLoading } = this.props;
    const { content } = this.state;

    return (
      <Body full>
        {!messages || messages.length === 0 ? (
          <Body
            p={15}
            refreshControl={
              <RefreshControl
                refreshing={loading}
                onRefresh={() => this.fetchNotifications()}
              />
            }
          >
            <Text h4 center m="15px 0 50px">Không có phản hồi nào.</Text>
          </Body>
        ) : (
          <FlatList
            inverted
            contentContainerStyle={styles.list}
            // refreshControl={
            //   <RefreshControl
            //     refreshing={loading}
            //     onRefresh={() => this.fetchMessages()}
            //   />
            // }
            data={messages}
            keyExtractor={(item, idx) => idx+""}
            renderItem={({ item: message }) => (
              <ListItem>
                <Text bold h4 m="0 0 4px">{message.sender}</Text>
                <Text h4 m="0 0 10px">{message.content}</Text>
                <Text gray small>
                  <TimeAgo time={message.time} />
                </Text>
              </ListItem>
            )}
          />
        )}
        <Row>
          <TextInput
            style={{ flex: 1, marginRight: 10 }}
            value={content}
            onChangeText={content => this.setState({ content })}
          />
          <Button
            title="Gửi"
            onPress={() => this.postMessage()} 
            disabled={postMessageLoading || !content}
          />
        </Row>
      </Body>
    );
  }
}

Messages.navigationOptions = ({ navigation }) => ({
  headerTitle: "Thông báo",
});

Messages.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  messages: messagesSelector(state),
  loading: fetchMessagesLoadingSelector(state),
  postMessageLoading: postMessageLoadingSelector(state),
  phoneNumber: phoneNumberSelector(state),
});

const mapDispatchToProps = dispatch => ({
  fetchMessages: (itemCode) => dispatch(fetchMessages(itemCode)),
  postMessage: message => dispatch(postMessage(message)),
});


export default connect(mapStateToProps, mapDispatchToProps)(Messages);
