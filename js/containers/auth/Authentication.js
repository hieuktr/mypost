import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ActivityIndicator } from 'react-native';

import { Body, BrandText } from '../../components';

import { phoneNumberSelector } from '../../modules/auth/selectors';

class Authentication extends Component {

  componentDidMount() {
    const { phoneNumber } = this.props;
    if (phoneNumber) {
      this.props.navigation.navigate("home_stack_with_modal");
    } else {
      this.props.navigation.navigate("welcome_screen");
    }
  }

  render() {
    return (
      <Body brand>
        <BrandText />
        <ActivityIndicator />
      </Body>
    );
  }
}

Authentication.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  phoneNumber: phoneNumberSelector(state),
});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Authentication);
