import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Body, Button, Text, BrandText, TextInput } from '../../components';

import { signIn } from '../../modules/auth/actions';
import { signInLoadingSelector, signInErrorSelector } from '../../modules/auth/selectors';

class Welcome extends Component {
  state = {
    phoneNumber: '',
    error: '',
  }
  
  onPhoneNumberChange(phoneNumber) {
    if (phoneNumber.length <= 10) {
      this.setState({ phoneNumber });
    }
  }

  signIn() {
    this.setState({ error: "" });
    const { phoneNumber } = this.state;
    if (phoneNumber.length === 10) {
      this.props.signIn(this.state.phoneNumber);
    } else {
      this.setState({ error: "Vui lòng nhập đúng số điện thoại." })
    }
  }

  render() {
    const { loading } = this.props;
    const { phoneNumber } = this.state;
    const error = this.state.error || this.props.error;

    return (
      <Body brand>
        <BrandText />
        <Text m="0 0 50px">Make your Post great</Text>
        <TextInput
          value={phoneNumber}
          onChangeText={phoneNumber => this.onPhoneNumberChange(phoneNumber)}
          placeholder="Nhập số điện thoại"
          m="0 0 20px"
          round
          keyboardType="phone-pad"
        />
        <Button
          title={loading ? "Đang xác thực..." : "Đăng nhập"}
          // onPress={() => navigate("home_stack")}
          onPress={() => this.signIn()}
          block
          round
          disabled={loading}
        />
        {error ? <Text secondary m="10px 0">{error}</Text> : null}
      </Body>
    );
  }
}

Welcome.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  loading: signInLoadingSelector(state),
  error: signInErrorSelector(state),
});

const mapDispatchToProps = dispatch => ({
  signIn: (phoneNumber) => dispatch(signIn(phoneNumber))
});

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
