import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createForm, formShape } from 'rc-form';
import { View } from 'react-native';
import styled from 'styled-components';
import { Icon } from 'react-native-elements';

import { Body, Progress, FormCard, Button, TextInputNinja } from '../../components';

const Box = styled.View`
  border: 1px dashed #FED37A;
  border-radius: ${({ theme }) => theme.other.radius}px;
  padding-bottom: 0;
  margin-bottom: 15px;
`;

const TextInputNinjaInBox = styled(TextInputNinja)`
  border: 0 solid ${({ theme }) => theme.color.lightgray};
  ${({ last }) => !last && `border-bottom-width: 1px;`}
`;

const Index = styled.Text`
  position: absolute;
  top: -5px;
  left: -5px;
  min-width: 20px;
  height: 20px;
  border: 1px solid ${({ theme }) => theme.color.primaryLight};
  border-radius: 3px;
  background-color: ${({ theme }) => theme.color.light};
  text-align: center;
`;

const DeleteButton = styled.TouchableOpacity`
  position: absolute;
  top: -5px;
  right: -5px;
  min-width: 20px;
  height: 20px;
  border: 1px solid ${({ theme }) => theme.color.error};
  border-radius: 3px;
  background-color: ${({ theme }) => theme.color.error};;
`;

class CreateOrder extends Component {

  state = {
    items: [{ id: 1 }, ]
  }

  constructor(props) {
    super(props);

    let items = [{ id: 1 }, ];
    if (props.navigation.state.params) {
      items = props.navigation.state.params.items.map((item, idx) => ({ id: idx }));
    }

    this.state = {
      items
    }
  }

  componentDidMount() {
    if (this.props.navigation.state.params) {
      const {
        senderName, senderPhone, senderAddress,
        receiverName, receiverPhone, receiverAddress,
        items,
      } = this.props.navigation.state.params;

      let fields = {
        senderName, senderPhone, senderAddress,
        receiverName, receiverPhone, receiverAddress,
      }
      
      this.state.items.forEach(({ id }, idx) => {
        fields[`description_${id}`] = items[idx].description;
        fields[`price_${id}`] = items[idx].price;
        fields[`weight_${id}`] = items[idx].weight;
        fields[`quantity_${id}`] = items[idx].quantity;
      })

      this.props.form.setFieldsValue(fields);
    }
  }

  handleNext() {
    this.props.form.validateFields((error, value) => {
      const {
        senderName, senderPhone, senderAddress,
        receiverName, receiverPhone, receiverAddress,
      } = value;

      const items = this.state.items.map((item, idx) => {
        return {
          id: item.id,
          description: value[`description_${item.id}`],
          price: value[`price_${item.id}`],
          weight: value[`weight_${item.id}`],
          quantity: value[`quantity_${item.id}`],
        }
      });

      if (!error) {
        console.log(value, items);
        this.props.navigation.navigate("confirm_create_order_screen", {
          senderName, senderPhone, senderAddress,
          receiverName, receiverPhone, receiverAddress,
          items,
        });
      }
    });
  }

  handleAddItem() {
    this.setState((state) => ({
      items: state.items.concat({
        id: new Date().toISOString()
      })
    }));
  }

  handleRemoveItem(index) {
    this.setState((state) => ({
      items: state.items
              .slice(0, index)
              .concat(state.items.slice(index + 1))
    }));
  }

  handleOpenAddressModal(field) {
    console.log(field, this.props.form.getFieldValue(field));
    this.props.navigation.navigate("address_search", {
      onConfirm: (value) => this.props.form.setFieldsValue({
        [field]: value,
      }),
      address: this.props.form.getFieldValue(field),
    })
  }
  
  render() {
    const { form } = this.props;
    const { getFieldProps, getFieldError, getFieldValue } = form;

    const senderInputs = [
      {
        ...getFieldProps('senderName', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập họ tên',
            },
          ],
        }),
        value: getFieldValue('senderName'),
        errors: getFieldError('senderName'),
        placeholder: 'Họ tên',
        autoCapitalize: 'words',
        icon: {
          name: 'user',
          type: 'font-awesome'
        }
      },
      {
        ...getFieldProps('senderPhone', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập số điện thoại',
            },
          ],
        }),
        value: getFieldValue('senderPhone'),
        errors: getFieldError('senderPhone'),
        placeholder: 'Số điện thoại',
        keyboardType: 'phone-pad',
        icon: {
          name: 'phone',
          type: 'font-awesome'
        }
      },
      {
        ...getFieldProps('senderAddress', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập địa chỉ',
            },
          ],
        }),
        value: getFieldValue('senderAddress'),
        errors: getFieldError('senderAddress'),
        placeholder: 'Địa chỉ',
        autoCapitalize: 'words',
        icon: {
          name: 'home',
          type: 'font-awesome'
        },
        onFocus: () => this.handleOpenAddressModal("senderAddress")
      },
    ];

    const receiverInputs = [
      {
        ...getFieldProps('receiverName', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập họ tên',
            },
          ],
        }),
        value: getFieldValue('receiverName'),
        errors: getFieldError('receiverName'),
        placeholder: 'Họ tên',
        autoCapitalize: 'words',
        icon: {
          name: 'user',
          type: 'font-awesome'
        }
      },
      {
        ...getFieldProps('receiverPhone', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập số điện thoại',
            },
          ],
        }),
        value: getFieldValue('receiverPhone'),
        errors: getFieldError('receiverPhone'),
        placeholder: 'Số điện thoại',
        keyboardType: 'phone-pad',
        icon: {
          name: 'phone',
          type: 'font-awesome'
        }
      },
      {
        ...getFieldProps('receiverAddress', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập địa chỉ',
            },
          ],
        }),
        value: getFieldValue('receiverAddress'),
        errors: getFieldError('receiverAddress'),
        placeholder: 'Địa chỉ',
        autoCapitalize: 'words',
        icon: {
          name: 'home',
          type: 'font-awesome'
        }
      },
    ];

    const items = this.state.items.map((item, idx) => ({
      ...item,
      inputs: [
        {
          ...getFieldProps(`description_${item.id}`, {
            rules: [
              {
                required: true, message: 'Vui lòng nhập mô tả',
              },
            ],
          }),
          value: getFieldValue(`description_${item.id}`),
          errors: getFieldError(`description_${item.id}`),
          placeholder: 'Mô tả',
          icon: {
            name: 'package',
            type: 'material-community'
          }
        },
        {
          ...getFieldProps(`price_${item.id}`, {
            rules: [
              {
                required: true, message: 'Vui lòng nhập số tiền',
              },
            ],
          }),
          value: getFieldValue(`price_${item.id}`),
          errors: getFieldError(`price_${item.id}`),
          placeholder: 'Giá trị thành tiền',
          keyboardType: 'numeric',
          icon: {
            name: 'cash',
            type: 'material-community'
          }
        },
        {
          ...getFieldProps(`weight_${item.id}`, {
            rules: [
              {
                required: true, message: 'Vui lòng nhập trọng lượng',
              },
            ],
          }),
          value: getFieldValue(`weight_${item.id}`),
          errors: getFieldError(`weight_${item.id}`),
          placeholder: 'Trọng lượng',
          keyboardType: 'numeric',
          icon: {
            name: 'weight-kilogram',
            type: 'material-community'
          }
        },
        {
          ...getFieldProps(`quantity_${item.id}`, {
            rules: [
              {
                required: true, message: 'Vui lòng nhập số lượng',
              },
            ],
          }),
          value: getFieldValue(`quantity_${item.id}`),
          errors: getFieldError(`quantity_${item.id}`),
          placeholder: 'Số lượng',
          keyboardType: 'numeric',
          icon: {
            name: 'page-multiple',
            type: 'foundation'
          }
        },
      ]
    }));

    return (
      <Body>
        <Progress
          steps={["Thông tin", "Xác nhận"]}
          currentStep={0}
          m="0 0 10px"
        />
        <FormCard title="Người gửi">
          {senderInputs.map((input, idx) => <TextInputNinja key={idx} {...input} />)}
        </FormCard> 
        <FormCard title="Người nhận">
          {receiverInputs.map((input, idx) => <TextInputNinja key={idx} {...input} />)}
        </FormCard>
        <FormCard title="Hàng hoá">
          {items.map((item, itemIdx) => 
            <View key={itemIdx}>
              <Box>
                {item.inputs.map((input, inputIdx) => 
                  <TextInputNinjaInBox
                    key={inputIdx}
                    {...input}
                    m="0"
                    noborder
                    last={inputIdx === item.inputs.length - 1}
                  />)}
              </Box>
              <Index>{itemIdx + 1}</Index>
              {itemIdx !== 0 &&
                <DeleteButton onPress={() => this.handleRemoveItem(itemIdx)}>
                  <Icon type="ionicon" name="ios-close" color="white" size={18} />
                </DeleteButton>}
            </View>
          )}
          <Button
            title="+ Thêm"
            onPress={() => this.handleAddItem()}
            transparent
          />
        </FormCard>
        <Button
          title="Tiếp tục"
          onPress={() => this.handleNext()}
          m="0 15px 15px"
          round
        />
      </Body>
    );
  }
}

CreateOrder.navigationOptions = ({ navigation }) => ({
  headerTitle: "Tạo đơn"
});

CreateOrder.propTypes = {
  navigation: PropTypes.object.isRequired,
  form: formShape,
};

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(createForm()(CreateOrder));
