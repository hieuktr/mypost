import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View } from 'react-native';
import styled from 'styled-components';

import { Text, Progress, FormCard, Button, TextInputNinja, Body } from '../../components';
import { addOrder } from '../../modules/order/actions';

const Item = styled.View`
  flex-direction: row;
  margin-bottom: 10px;
`;

class ConfirmCreateOrder extends Component {

  constructor(props) {
    super(props);

    const price = props.navigation.state.params.items
      .reduce((accumulator, currentItem) => accumulator + currentItem.price, 0);

    this.state = {
      price,
      note: '',
    }
  }

  onConfirm() {
    const { navigation: { navigate, state }, addOrder } = this.props;
    const { price, note } = this.state;
    const {
      senderName, senderPhone, senderAddress,
      receiverName, receiverPhone, receiverAddress,
      items,
    } = state.params;
    
    addOrder({
      senderName, senderPhone, senderAddress,
      receiverName, receiverPhone, receiverAddress,
      items,
      price, note,
    });
    navigate("order_screen");
  }
  
  render() {
    const { navigation } = this.props;
    const { price, note } = this.state;
    const {
      senderName, senderPhone, senderAddress,
      receiverName, receiverPhone, receiverAddress,
      items,
    } = navigation.state.params;
    return (
      <Body>
        <Progress
          steps={["Thông tin", "Xác nhận"]}
          currentStep={1}
          m="0 0 10px"
        />
        <FormCard>
          <Text>Tổng cước: <Text bold>20.000 đ</Text></Text>
          <Text>Thời gian giao dự kiến: <Text bold>12 giờ</Text></Text>
        </FormCard>
        <FormCard title="Tiền thu hộ">
          <TextInputNinja
            value={price}
            onChangeText={price => this.setState({ price })}
            placeholder="Số tiền"
            icon={{
              name: 'cash',
              type: 'material-community',
              size: 22,
            }}
          />
        </FormCard> 
        <FormCard title="Ghi chú">
          <TextInputNinja
            value={note}
            onChangeText={note => this.setState({ note })}
            placeholder="Ghi chú"
            icon={{
              name: 'edit',
              type: 'entypo',
              size: 17,
            }}
          />
        </FormCard>
        <FormCard title="Người gửi">
          <Text>{`${senderName} - ${senderPhone}`}</Text>
          <Text>{senderAddress}</Text>
        </FormCard>
        <FormCard title="Người nhận">
          <Text>{`${receiverName} - ${receiverPhone}`}</Text>
          <Text>{receiverAddress}</Text>
        </FormCard>
        <FormCard title="Hàng hoá">
          {items.map((item, idx) => (
            <Item key={idx}>
              <Text>{idx+1}. </Text>
              <View style={{ flex: 1 }}>
                <Text bold m="0 0 5px">{item.description}</Text>
                <Text error>{item.price} đ</Text>
              </View>
              <View>
                <Text m="0 0 5px">{item.weight} Kg</Text>
                <Text>{item.quantity} Chiếc</Text>
              </View>
            </Item>
          ))}
        </FormCard>
        <Button
          title="Gửi"
          onPress={() => this.onConfirm()}
          m="0 15px 15px"
          round
        />
      </Body>
    );
  }
}

ConfirmCreateOrder.navigationOptions = ({ navigation }) => ({
  headerTitle: "Xác nhận đơn hàng"
});

ConfirmCreateOrder.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  addOrder: order => dispatch(addOrder(order)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmCreateOrder);
