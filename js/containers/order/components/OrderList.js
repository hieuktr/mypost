import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FlatList, ScrollView, RefreshControl, View } from 'react-native';
import StatusFilter from './StatusFilter';
import { Text } from '../../../components';

class OrderList extends Component {
  
  render() {
    const {
      orders,
      loading,
      onRefresh,
      renderItem,
      filterOptions,
      filterDefaultValue,
      filterDefaultIndex,
      onFilterPress,
    } = this.props;

    return (
      <View>
        <StatusFilter
            options={filterOptions}
            defaultValue={`${filterDefaultValue} (${orders.length})`}
            defaultIndex={filterDefaultIndex}
            onSelect={onFilterPress}
          />
          {!orders || orders.length === 0 ? (
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={loading}
                  onRefresh={onRefresh}
                />
              }
            >
              <Text h4 m="15px 0 50px" center>Không có đơn hàng nào.</Text>
            </ScrollView>
          ) : (
            <FlatList
              refreshControl={
                <RefreshControl
                  refreshing={loading}
                  onRefresh={onRefresh}
                />
              }
              data={orders}
              keyExtractor={(item, idx) => idx+""}
              renderItem={renderItem}
            />
          )}
      </View>
    );
  }
}

OrderList.propTypes = {
  orders: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  onRefresh: PropTypes.func.isRequired,
  renderItem: PropTypes.func.isRequired,
  filterOptions: PropTypes.array,
  filterDefaultValue: PropTypes.string,
  filterDefaultIndex: PropTypes.number,
  onFilterPress: PropTypes.func,
};

export default OrderList;
