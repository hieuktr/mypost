import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import { Text, StatusTag } from '../../../components';

const ListItem = styled.View`
  background-color: white;
  padding: 15px;
  border: 0 solid ${({ theme }) => theme.color.lightgray};
  border-bottom-width: 1px;
`;

class OrderListItem extends Component {
  render() {
    const { itemCode, name, phone, address, statusCode, onPress } = this.props;

    return (
      <ListItem>
        <TouchableOpacity onPress={onPress}>
          <Text bold h3 m="0 0 12px">{itemCode}</Text>
          <Text h4 m="0 0 8px">{`${name}${phone ? ` (${phone})`: ''}`}</Text>
          <Text h4 m="0 0 8px">{address}</Text>
          <StatusTag statusCode={statusCode} />
        </TouchableOpacity>
      </ListItem>
    );
  }
}

OrderListItem.propTypes = {
  itemCode: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  statusCode: PropTypes.string.isRequired,
};

export default OrderListItem;
