import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import theme from '../../../config/theme';

class StatusFilter extends Component {
  
  render() {
    const { options, defaultValue, defaultIndex, onSelect } = this.props;
    return (
      <ModalDropdown
        options={options}
        defaultValue={defaultValue}
        defaultIndex={defaultIndex}
        onSelect={onSelect}
        style={{
          padding: 5,
          paddingHorizontal: 15,
          margin: 10,
          backgroundColor: 'white',
          borderRadius: 20,
          borderColor: theme.color.primary,
        }}
        textStyle={{
          fontSize: theme.typo.h4.size,
          fontFamily: theme.font.primary,
        }}
        dropdownStyle={{
          width: 250,
          // height: 401,
          borderColor: theme.color.primary,
          borderWidth: 2,
        }}
        dropdownTextStyle={{
          fontSize: theme.typo.h4.size,
          fontFamily: theme.font.primaryMedium,
          borderTopColor: theme.color.primary,
        }}
        dropdownTextHighlightStyle={{
          color: theme.color.primary,
          fontFamily: theme.font.primaryBold,
        }}
        renderSeparator={() => (
          <View style={{
            height: 1,
            backgroundColor: theme.color.primary,
          }}/>
        )}
      />
    );
  }
}

StatusFilter.propTypes = {
  options: PropTypes.array.isRequired,
  defaultValue: PropTypes.string.isRequired,
  defaultIndex: PropTypes.number.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default StatusFilter;
