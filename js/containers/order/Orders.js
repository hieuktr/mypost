import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, Dimensions } from 'react-native';
// import { FloatingAction } from 'react-native-floating-action';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import PushNotification from 'react-native-push-notification';

import OrderList from './components/OrderList';
import OrderListItem from './components/OrderListItem';

import { fetchOrders } from '../../modules/order/actions';
import { fetchNotifications } from '../../modules/notification/actions';
import { updateFCMToken } from '../../modules/user/actions';
import {
  sendOrdersSelector,
  receiveOrdersSelector,
  fetchOrdersLoadingSelector,
  fetchOrdersErrorSelector,
} from '../../modules/order/selectors';
import theme from '../../config/theme';
import { STATUS_NAME_TO_CODE } from '../../config/app';

const filterOptions = Object.keys(STATUS_NAME_TO_CODE);

// const actions = [
//   {
//     text: 'Đơn hàng mẫu',
//     name: 'create_order_screen_sample',
//     route: 'create_order_screen',
//     color: theme.color.primary,
//     textBackground: 'transparent',
//     textElevation: 0,
//     position: 1,
//   },
//   {
//     text: 'Tạo đơn hàng',
//     name: 'create_order_screen',
//     route: 'create_order_screen',
//     color: theme.color.primary,
//     textBackground: 'transparent',
//     textElevation: 0,
//     position: 2
//   },
// ];

// const actionData = {
//   create_order_screen_sample: {
//     route: "create_order_screen",
//     params: {
//       senderName: "Nguyễn Văn A",
//       senderPhone: "0912345678",
//       senderAddress: "Trần Duy Hưng, Hà Nội",
//       receiverName: "Nguyễn Văn B",
//       receiverPhone: "0987654321",
//       receiverAddress: "Cầu Giấy, Hà Nội",
//       items: [
//         {
//           description: "Máy in HP",
//           price: 2000000,
//           weight: 3,
//           quantity: 1
//         },
//         {
//           description: "Mực in",
//           price: 500000,
//           weight: 1,
//           quantity: 1
//         },
//       ]
//     }
//   },
//   create_order_screen: {
//     route: "create_order_screen",
//   }
// }

class Orders extends Component {

  state = {
    index: 0,
    routes: [
      { key: 'send', title: 'GỬI ĐI' },
      { key: 'receive', title: 'GỬI ĐẾN' },
    ],
    filterValue: filterOptions[0],
    filterIndex: 0,
  };

  componentDidMount() {
    this.fetchOrders();
    this.fetchNotifications();

    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: (token) => {
          console.log( 'TOKEN:', token );
          this.props.updateFCMToken(token.token);
      },

      // (required) Called when a remote or local notification is opened or received
      onNotification: (notification) => {
          console.log( 'NOTIFICATION:', notification );
          this.fetchOrders();
          this.fetchNotifications();

          // process the notification

          // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
          // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
      senderID: "867329889145",

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
          alert: true,
          badge: true,
          sound: true
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
        * (optional) default: true
        * - Specified if permissions (ios) and token (android and ios) will requested or not,
        * - if not, you must call PushNotificationsHandler.requestPermissions() later
        */
      requestPermissions: true,
    });
  }

  onPress(order) {
    this.props.navigation.navigate('order_details_stack', { order });
  }
  
  fetchOrders() {
    this.props.fetchOrders();
  }

  fetchNotifications() {
    this.props.fetchNotifications();
  }

  onFilterPress(index, value) {
    // const statusValue = ITEM_STATUSES[value];
    // console.log(statusValue);
    this.setState({ filterValue: value, filterIndex: parseInt(index, 10) });
    return true;
  }

  render() {
    const { sendOrders, receiveOrders, loading, error } = this.props;
    const { filterValue, filterIndex } = this.state;

    return (
      <View style={{ flex: 1}}>
        <TabView
          navigationState={this.state}
          initialLayout={{
            height: 0,
            width: Dimensions.get('window').width,
          }}
          renderScene={SceneMap({
            send: () => (
              <OrderList
                renderItem={({ item: order }) => (
                  <OrderListItem
                    {...order}
                    name={order.receiverName}
                    phone={order.receiverPhone}
                    address={order.receiverAddress}
                    onPress={() => this.onPress(order)}
                  />
                )}
                orders={sendOrders.filter(order => {
                  if (STATUS_NAME_TO_CODE[filterValue] === "0") {
                    return true;
                  } else if (STATUS_NAME_TO_CODE[filterValue] === order.statusCode) {
                    return true;
                  }
                })}
                loading={loading}
                onRefresh={() => this.fetchOrders()}
                filterOptions={filterOptions}
                filterDefaultValue={filterValue}
                filterDefaultIndex={filterIndex}
                onFilterPress={(index, value) => this.onFilterPress(index, value)}
              />
            ),
            receive: () => (
              <OrderList
                renderItem={({ item: order }) => (
                  <OrderListItem
                    {...order}
                    name={order.senderName}
                    phone={order.senderPhone}
                    address={order.senderAddress}
                    onPress={() => this.onPress(order)}
                  />
                )}
                orders={receiveOrders.filter(order => {
                  if (STATUS_NAME_TO_CODE[filterValue] === "0") {
                    return true;
                  } else if (STATUS_NAME_TO_CODE[filterValue] === order.statusCode) {
                    return true;
                  }
                })}
                loading={loading}
                onRefresh={() => this.fetchOrders()}
                filterOptions={filterOptions}
                filterDefaultValue={filterValue}
                filterDefaultIndex={filterIndex}
                onFilterPress={(index, value) => this.onFilterPress(index, value)}
              />
            ),
          })}
          onIndexChange={index => this.setState({ index })}
          renderTabBar={props =>
            <TabBar
              {...props}
              scrollEnabled={true}
              bounces={true}
              getLabelText={({ route: { key, title } }) => title}
              style={{
                elevation: 0,
                backgroundColor: theme.color.secondary,
              }}
              tabStyle={{
                backgroundColor: "transparent",
                paddingVertical: 3,
                // width: 130,
                width: Dimensions.get('window').width/2
              }}
              labelStyle={{
                fontFamily: theme.font.primaryBold,
                fontSize: theme.typo.p.size,
              }}
              indicatorStyle={{
                backgroundColor: theme.color.primary,
                height: 3,
              }}
            />
          }
        />
        
        {/*<FloatingAction
          color={theme.color.secondary}
          actions={actions}
          onPressItem={
            (name) => {
              navigate(actionData[name].route, actionData[name].params);
            }
          }
        />*/}
      </View>
    );
  }
}

Orders.navigationOptions = ({ navigation }) => ({
  headerTitle: "Bưu gửi",
});

Orders.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  sendOrders: sendOrdersSelector(state),
  receiveOrders: receiveOrdersSelector(state),
  loading: fetchOrdersLoadingSelector(state),
  error: fetchOrdersErrorSelector(state),
});

const mapDispatchToProps = dispatch => ({
  fetchOrders: () => dispatch(fetchOrders()),
  fetchNotifications: () => dispatch(fetchNotifications()),
  updateFCMToken: (fcmToken) => dispatch(updateFCMToken(fcmToken)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
