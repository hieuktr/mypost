import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Text, Body, FormCard, StatusTag, Button } from '../../components';

import { parseNumberWithDot } from '../../utils';

class OrderDetails extends Component {

  onSendReply() {
    this.props.navigation.navigate('messages_screen', { itemCode: this.props.navigation.state.params.order.itemCode });
  }

  render() {
    const {
      itemCode, statusCode, deliveryTracks,
      sendingContent, totalWeight,
      senderAmount, receiverAmount, totalFreight, totalCollect,
      senderName, senderPhone, senderAddress,
      receiverName, receiverPhone, receiverAddress,
    } = this.props.navigation.state.params.order;
    return (
      <Body>
        <FormCard>
          <Text bold h4 m="0 0 15px">{itemCode}</Text>
          <StatusTag statusCode={statusCode} />
        </FormCard>
        {deliveryTracks && deliveryTracks.length > 0 ? (
          <FormCard title="Thông tin vận chuyển">
            {deliveryTracks.map((track, idx) => (
              <Text key={idx} h4 m="0 0 15px">{`• ${track.time.replace("T", " ").slice(0, -4)} - ${track.status}`}</Text>
            ))}
          </FormCard>
        ) : null}
        <FormCard title="Thông tin bưu gửi">
          <Text h4 m="0 0 15px">{sendingContent}</Text>
          <Text h4>Trọng lượng: <Text bold h4>{parseNumberWithDot(totalWeight)} g</Text></Text>
        </FormCard>
        <FormCard title="Thông tin thu hộ">
          <Text h4 m="0 0 15px">• Cước vận chuyển: <Text h4 bold>{parseNumberWithDot(totalFreight)} đ</Text></Text>
          <Text h4 m="0 0 15px">• Số tiền thu hộ: <Text h4 bold>{parseNumberWithDot(totalCollect)} đ</Text></Text>
          <Text h4 m="0 0 15px">• Số tiền thu tại người gửi: <Text h4 bold>{parseNumberWithDot(senderAmount)} đ</Text></Text>
          <Text h4>• Số tiền thu tại người nhận: <Text h4 bold>{parseNumberWithDot(receiverAmount)} đ</Text></Text>
        </FormCard>
        <FormCard title="Người gửi">
          <Text h4 m="0 0 15px">{`${senderName}${senderPhone ? ` - ${senderPhone}`: ''}`}</Text>
          <Text h4>{senderAddress}</Text>
        </FormCard>
        <FormCard title="Người nhận">
          <Text h4 m="0 0 15px">{`${receiverName}${receiverPhone ? ` - ${receiverPhone}`: ''}`}</Text>
          <Text h4>{receiverAddress}</Text>
        </FormCard>
        {/*<FormCard title="Bưu tá">
          <Text h4 m="0 0 15px">Nguyễn Văn A</Text>
          <Text h4>0966666666</Text>
        </FormCard>*/}
        <Button
          title="Gửi phản hồi"
          onPress={() => this.onSendReply()}
          m="0 15px 15px"
          round
        />
      </Body>
    );
  }
}

OrderDetails.navigationOptions = ({ navigation }) => ({
  headerTitle: "Chi tiết bưu gửi",
});

OrderDetails.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetails);
