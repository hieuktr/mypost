import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, RefreshControl } from 'react-native';
import styled from 'styled-components';
import { PieChart } from 'react-native-svg-charts'

import { Body, Text, FormCard } from '../../components';
import { fetchHistory } from '../../modules/history/actions';
import { historySelector, fetchHistoryLoadingSelector } from '../../modules/history/selectors';

import theme from '../../config/theme';
import { parseNumberWithDot } from '../../utils';

const Row = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 10px;
`;

const Color = styled.View`
  height: 10px;
  width: 10px;
  background-color: ${({ color }) => color || 'white'};
  border-radius: 20px;
  margin-right: 10px;
`;

class History extends Component {

  componentDidMount() {
    this.fetchHistory();
  }

  fetchHistory() {
    this.props.fetchHistory();
  }

  render() {
    const { history: { thongke, taichinh }, loading } = this.props;

    if (!thongke || !taichinh) {
      return (
        <Body
            p={15}
            refreshControl={
              <RefreshControl
                refreshing={loading}
                onRefresh={() => this.fetchHistory()}
              />
            }
          >
            <Text h4 center m="15px 0 50px">Chưa có thông tin.</Text>
          </Body>
      )
    }

    const { chotratien, chuatratien, datratien } = taichinh;
    const { choduyet, danggiao, thanhcong, dahuy } = thongke;

    const data = [
      {
        name: "Chờ duyệt",
        color: theme.color.warning,
        value: choduyet,
      },
      {
        name: "Đang giao",
        color: theme.color.standard,
        value: danggiao,
      },
      {
        name: "Thành công",
        color: theme.color.success,
        value: thanhcong,
      },
      {
        name: "Đã huỷ",
        color: theme.color.error,
        value: dahuy,
      },
    ];

    const pieData = data
        .filter(item => item.value > 0)
        .map((item, index) => ({
            value: item.value,
            svg: {
                fill: item.color,
                onPress: () => console.log('press ', index),
            },
            key: `pie-${index}`,
        }))

    return (
      <Body
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={() => this.fetchHistory()}
          />
        }
      >
        <FormCard title={<Text h3 m="0 0 20px">Thống kê</Text>}>
          <Row>
            <PieChart
              style={{ height: 150, flex: 1 }}
              data={ pieData }
            />
            <View>
              {data.map(item => 
                <Row key={item.name}>
                  <Color color={item.color} />
                  <Text>{item.name} ({item.value})</Text>
                </Row>
              )}
            </View>
          </Row>
        </FormCard>
        <FormCard title={<Text h3 m="0 0 20px">Tài chính</Text>}>
          <Row>
            <Text bold flex>Chưa trả tiền</Text>
            <Text>{parseNumberWithDot(chuatratien)} đ</Text>
          </Row>
          <Row>
            <Text bold flex>Chờ trả tiền</Text>
            <Text>{parseNumberWithDot(chotratien)} đ</Text>
          </Row>
          <Row>
            <Text bold flex>Đã trả tiền</Text>
            <Text>{parseNumberWithDot(datratien)} đ</Text>
          </Row>
        </FormCard>
      </Body>
    );
  }
}

History.navigationOptions = ({ navigation }) => ({
  headerTitle: "Lịch sử",
});

History.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  history: historySelector(state),
  loading: fetchHistoryLoadingSelector(state),
});

const mapDispatchToProps = dispatch => ({
  fetchHistory: () => dispatch(fetchHistory())
});


export default connect(mapStateToProps, mapDispatchToProps)(History);
