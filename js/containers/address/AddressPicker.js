import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createForm, formShape } from 'rc-form';
import { TextInputNinja, Button, Body } from '../../components';

class AddressPicker extends Component {

  componentDidMount() {
    this.props.form.setFieldsValue({
      unit: this.props.navigation.state.params.unit,
      district: this.props.navigation.state.params.district,
      province: this.props.navigation.state.params.province,
      city: this.props.navigation.state.params.city,
    });
  }

  onConfirm() {
    this.props.form.validateFields((error, value) => {
      if (!error) {
        const { unit, district, province, city } = value;
        let address = `${unit ? unit+', ' : ''}`;
        address += `${district ? district+', ' : ''}`;
        address += `${province ? province+', ' : ''}`;
        address += `${city ? city : ''}`;
        this.props.navigation.state.params.onConfirm(address);
        this.props.navigation.goBack(null);
        this.props.navigation.goBack(null);
      }
    });
  }
  
  render() {
    const { form } = this.props;
    const { getFieldProps, getFieldError, getFieldValue } = form;

    const inputs = [
      {
        ...getFieldProps('city', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập Tỉnh/Thành phố',
            },
          ],
        }),
        value: getFieldValue('city'),
        errors: getFieldError('city'),
        placeholder: 'Tỉnh/Thành phố',
        autoCapitalize: 'words',
        icon: {
          name: 'city',
          type: 'material-community'
        }
      },
      {
        ...getFieldProps('province', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập Quận/Huyện',
            },
          ],
        }),
        value: getFieldValue('province'),
        errors: getFieldError('province'),
        placeholder: 'Quận/Huyện',
        autoCapitalize: 'words',
        icon: {
          name: 'desktop-tower',
          type: 'material-community'
        }
      },
      {
        ...getFieldProps('district', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập Phường/Xã',
            },
          ],
        }),
        value: getFieldValue('district'),
        errors: getFieldError('district'),
        placeholder: 'Phường/Xã',
        autoCapitalize: 'words',
        icon: {
          name: 'road',
          type: 'font-awesome'
        },
      },
      {
        ...getFieldProps('unit', {
          rules: [
            {
              required: true, message: 'Vui lòng nhập Số nhà/Ngõ/Thôn/Xóm',
            },
          ],
        }),
        value: getFieldValue('unit'),
        errors: getFieldError('unit'),
        placeholder: 'Số nhà/Ngõ/Thôn/Xóm',
        autoCapitalize: 'words',
        icon: {
          name: 'home',
          type: 'font-awesome'
        },
      },
    ];

    return (
      <Body p={10}>
        {inputs.map((input, idx) => <TextInputNinja key={idx} {...input} />)}
        <Button
          title="Xác nhận"
          onPress={() =>this.onConfirm()}
        />
      </Body>
    );
  }
}

AddressPicker.navigationOptions = ({ navigation }) => ({
  headerTitle: "Xác nhận địa chỉ"
});

AddressPicker.propTypes = {
  navigation: PropTypes.object.isRequired,
  form: formShape,
};

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(createForm()(AddressPicker));
