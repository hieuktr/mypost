import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Body } from '../../components';
import theme from '../../config/theme';

class AddressSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      address: props.navigation.state.params.address || '',
    }
  }

  onSelect(data, details) {
    const { navigation } = this.props;
    if (details) {
      const { address_components } = details;
      let address = {};
      address_components.forEach(component => {
        let type;
        if (component.types[0] === "street_number") {
          type = 'unit';
        } else if (component.types[0] === "route" || component.types[0] === "neighborhood" || component.types[0] === "sublocality_level_1") {
          type = 'district';
        } else if (component.types[0] === "administrative_area_level_2" || component.types[0] === "locality") {
          type = 'province';
        } else if (component.types[0] === "administrative_area_level_1") {
          type = 'city';
        }
        address[type] = !address[type] ? component.long_name : `${address[type]} ${component.long_name}`;
      });

      navigation.navigate("address_picker", {
        onConfirm: navigation.state.params.onConfirm,
        unit: address.unit,
        district: address.district,
        province: address.province,
        city: address.city,
      });
    }
  }
  
  render() {
    return (
      <Body p={10}>
        <GooglePlacesAutocomplete
          placeholder='Nhập từ khoá địa chỉ tìm kiếm'
          minLength={2}
          autoFocus={true}
          autoCorrect={true}
          listViewDisplayed='auto'
          fetchDetails={true}
          renderDescription={(row) => row.description}
          onPress={(data, details = null) => {
            console.log(data);
            console.log(details);
            this.onSelect(data, details);
          }}
          getDefaultValue={() => {
            return this.state.address;      // text input default value
          }}
          query={{
            // available options: https://developers.google.com/places/web-service/autocomplete
            key: 'AIzaSyAUYfbKtctkIibOgFnNN2x9Xg9i0sVxlhQ',
            language: 'vi',
            types: 'geocode',
            components: 'country:VN'
          }}
          styles={{
            container: {
            },
            loader: {
              marginRight: 10,
            },
            separator: {
              backgroundColor: theme.color.primaryLight,
            },
            description: {
              fontWeight: 'bold',
            },
            predefinedPlacesDescription: {
              color: '#1faadb',
            },
            textInputContainer: {
              backgroundColor: 'rgba(0,0,0,0)',
              borderTopWidth: 0,
              borderBottomWidth: 0,
              height: 45,
            },
            textInput: {
              height: 45,
              paddingLeft: 15,
              borderWidth: 1,
              borderRadius: 5,
              borderColor: theme.color.primaryLight,
              marginTop: 0,
              marginBottom: 0,
              marginLeft: 0,
              marginRight: 0,
            },
            listView: {
              top: 10,
              backgroundColor: 'white',
            },
            poweredContainer: {
              height: 0,
              marginTop: -22
            },
          }}

          currentLocation={false}
          currentLocationLabel="Current location"
          nearbyPlacesAPI='GooglePlacesSearch'
          GoogleReverseGeocodingQuery={{
              // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
          }}
          GooglePlacesSearchQuery={{
              // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
              rankby: 'distance',
              // types: 'food'
          }}

          filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3', 'sublocality', 'administrative_area_level_2', 'administrative_area_level_1']}
          // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
          debounce={200}
          //renderLeftButton={() => <Image source={require('left-icon')} />}
          // renderLeftButton={() => <Text>A</Text> }
          // renderRightButton={() => <Text>B</Text> }
        />
      </Body>
    );
  }
}

AddressSearch.navigationOptions = ({ navigation }) => ({
  headerTitle: "Nhập địa chỉ"
});

AddressSearch.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(AddressSearch);
