import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, autoRehydrate } from 'redux-persist';
import { AsyncStorage } from 'react-native';
import immutableTransform from 'redux-persist-transform-immutable';
import { navigationReduxMiddleware } from './navigators/redux';
import rootReducer from './reducers';
import rootSaga from './sagas';

const persistConfig = {
  transforms: [immutableTransform({ blacklist: ['nav'] })],
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['nav', 'order', 'message', 'notification'],
  // whitelist: ['auth', 'user', 'order'],
};

const composeEnhancers = process.env.NODE_ENV === 'development' ? composeWithDevTools({ realtime: true }) : compose;
const sagaMiddleware = createSagaMiddleware();

export default function configureStore(onCompletion) {
  const enhancer = composeEnhancers(
    applyMiddleware(
      sagaMiddleware,
      navigationReduxMiddleware,
    ),
    autoRehydrate(),
  );

  const store = createStore(
    rootReducer,
    enhancer,
  );
  persistStore(store, persistConfig, onCompletion);

  // then run the saga
  sagaMiddleware.run(rootSaga);

  return store;
}
