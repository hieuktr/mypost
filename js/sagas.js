import { all } from 'redux-saga/effects';
import authSaga from './modules/auth/saga';
import userSaga from './modules/user/saga';
import orderSaga from './modules/order/saga';
import historySaga from './modules/history/saga';
import notificationSaga from './modules/notification/saga';
import messageSaga from './modules/message/saga';

export default function* rootSagas() {
  yield all([
    authSaga(),
    userSaga(),
    orderSaga(),
    historySaga(),
    notificationSaga(),
    messageSaga(),
  ]);
}
