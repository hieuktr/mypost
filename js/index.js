import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { SafeAreaView, StatusBar } from 'react-native';
import { ThemeProvider } from 'styled-components';
import configureStore from './configureStore';
import AppWithNavigationState from './navigators/AppWithNavigationState';
import Splash from './components/Splash';
import theme from './config/theme';

// console.ignoredYellowBox = ['Setting a timer'];

var moment = require('moment'); //load moment module to set local language
require('moment/locale/vi'); //for import moment local language file during the application build
moment.locale('vi');//set moment local language to zh-cn

class AppProvider extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      store: configureStore(() => this.setState({ isLoading: false })),
    };
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar backgroundColor={theme.color.primary} />
          {this.state.isLoading ? (
            <Splash />
          ) : (
            <Provider store={this.state.store}>
              <AppWithNavigationState />
            </Provider>
          )}
        </SafeAreaView>
      </ThemeProvider>
    );
  }
}

export default AppProvider;
