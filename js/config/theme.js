export default theme = {
    color: {
        primary: "#F6AC3D", // "#FAB237"
        secondary: "#C43B3D", // "#D7312E"
        primaryLight: "#FED37A",
        secondaryDark: "#6A2533",
        light: "#FFF",
        dark: "#333",
        lightgray: "#E2E2E2",
        gray: "#999",
        error: "#D7312E",
        warning: "#F6AC3D",
        success: "#A3BE28",
        standard: "#4688F1",
    },
    typo: {
        p: {
            size: 14,
            lineHeight: 16,
        },
        h1: {
            size: 30,
            lineHeight: 35,
        },
        h2: {
            size: 20,
            lineHeight: 25,
        },
        h3: {
            size: 18,
            lineHeight: 20,
        },
        h4: {
            size: 16,
            lineHeight: 18,
        },
        small: {
            size: 12,
            lineHeight: 14,
        },
    },
    font: {
        primary: "Quicksand-Regular",
        primaryLight: "Quicksand-Light",
        primaryMedium: "Quicksand-Medium",
        primaryBold: "Quicksand-Bold",
        secondary: "OpenSans-Regular",
        secondarySemiBold: "OpenSans-Semibold",
        secondaryBold: "OpenSans-Bold",
        secondaryExtraBold: "OpenSans-ExtraBold",
        brand: "RubikMonoOne-Regular",
    },
    other: {
        radius: 5
    }
}