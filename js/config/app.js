export const APP_NAME = 'iPost';
export const APP_API_URL = 'https://hcconline.vnpost.vn/ipost/webApiService/v1';

export const STATUS_NAME_TO_CODE = {
    "Tất cả": "0",
    "Tiếp nhận": "1",
    "Đang vận chuyển": "2",
    "Đang phát": "3",
    "Phát thành công": "4",
    "Phát không thành công": "5",
    "Bắt đầu chuyển hoàn": "6",
    "Phát hoàn thành công": "7",
    "Hủy": "8",
}

export const STATUSES = {
    "0": {
        name: "Tất cả",
    },
    "1": {
        name: "Tiếp nhận",
        color: "#4688F1",
    },
    "2": {
        name: "Đang vận chuyển",
        color: "#F6AC3D",
    },
    "3": {
        name: "Đang phát",
        color: "#F6AC3D",
    },
    "4": {
        name: "Phát thành công",
        color: "#A3BE28",
    },
    "5": {
        name: "Phát không thành công",
        color: "#D7312E",
    },
    "6": {
        name: "Bắt đầu chuyển hoàn",
        color: "#F6AC3D",
    },
    "7": {
        name: "Phát hoàn thành công",
        color: "#A3BE28",
    },
    "8": {
        name: "Hủy",
        color: "#D7312E",
    },
}