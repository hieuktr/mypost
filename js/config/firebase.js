import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';

const FIREBASE_CONFIG = {
  apiKey: "AIzaSyCT5RKcHDRFVbz_7gIBk6cEHMRwryd3CQE",
  authDomain: "mypost-2f156.firebaseapp.com",
  databaseURL: "https://mypost-2f156.firebaseio.com",
  projectId: "mypost-2f156",
  storageBucket: "mypost-2f156.appspot.com",
  messagingSenderId: "867329889145"
};

// Initialize the default app
export const app = firebase.initializeApp(FIREBASE_CONFIG);
export const auth = app.auth();
export const firestore = app.firestore();
firestore.settings({ timestampsInSnapshots: true });