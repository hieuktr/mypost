export const THEME_COLOR = '#FC563F';
export const HEART_COLOR = '#FF0048';
export const BACKGROUND_COLOR = '#F8F8F9';
export const BORDER_COLOR = '#C9C6D2';
export const BLACK_COLOR = '#222831';
export const GRAY_COLOR = '#9B9B9B';
export const LIGHTGRAY_COLOR = '#f2f2f2';
export const LIGHT_COLOR = '#fff8f6';
export const SUCCESS_COLOR = '#22D2A8';
export const FAIL_COLOR = '#FC0D1B';
export const FACEBOOK_COLOR = '#3D5A96';
export const GOOGLE_COLOR = '#DD4B39';

export const TEXT_COLOR = BLACK_COLOR;

export const FONT_FAMILY_PRIMARY = 'SFProDisplay-Regular';
export const FONT_FAMILY_PRIMARY_SEMI = 'SFProDisplay-Semibold';
export const FONT_FAMILY_PRIMARY_BOLD = 'SFProDisplay-Bold';
export const FONT_FAMILY_PRIMARY_HEAVY = 'SFProDisplay-Heavy';

export const FONT_FAMILY_SECOND = 'OpenSans-Regular';
export const FONT_FAMILY_SECOND_SEMI = 'OpenSans-Semibold';
export const FONT_FAMILY_SECOND_BOLD = 'OpenSans-Bold';
export const FONT_FAMILY_SECOND_HEAVY = 'OpenSans-ExtraBold';

export const FONT_SIZE = 14;
export const FONT_SIZE_TINY = 10;
export const FONT_SIZE_SMALL = 12;
export const FONT_SIZE_H4 = 16;
export const FONT_SIZE_H3 = 18;
export const FONT_SIZE_H2 = 20;
export const FONT_SIZE_H1 = 36;
