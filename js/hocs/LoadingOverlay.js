
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, ActivityIndicator } from 'react-native';

const styles = StyleSheet.create({
  loadingLayout: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
  },
  loadingWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginBottom: 5,
  },
});

class LoadingOverlay extends Component {
  render() {
    const { title, loading, children } = this.props;
    return (
      <View>
        {children}
        {loading ? (
          <View style={styles.loadingLayout}>
            <View style={styles.loadingWrapper}>
              <Text style={styles.title}>{title}</Text>
              <ActivityIndicator color="gray" />
            </View>
          </View>
        ) : null}
      </View>
    );
  }
}

LoadingOverlay.defaultProps = {
  title: 'Loading...',
};

LoadingOverlay.propTypes = {
  title: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  children: PropTypes.any,
};

export default LoadingOverlay;
