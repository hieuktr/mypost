import React from 'react';
import { View } from 'react-native';
import {
  createSwitchNavigator,
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';
import { Icon } from 'react-native-elements';
import Authentication from '../containers/auth/Authentication';
import Welcome from '../containers/auth/Welcome';
import Orders from '../containers/order/Orders';
import OrderDetails from '../containers/order/OrderDetails';
import CreateOrder from '../containers/order/CreateOrder';
import ConfirmCreateOrder from '../containers/order/ConfirmCreateOrder';
import Messages from '../containers/message/Messages';
import AddressSearch from '../containers/address/AddressSearch';
import AddressPicker from '../containers/address/AddressPicker';
import History from '../containers/history/History';
import Notifications from '../containers/notification/Notifications';
import NotificationIcon from '../containers/notification/NotificationIcon';
import Settings from '../containers/setting/Settings';
import BackIcon from '../components/header/BackIcon';
import theme from '../config/theme';

const navigationOptions = {
  headerStyle: {
    elevation: 0,
  },
  headerTitleStyle: {
    fontSize: theme.typo.h3.size,
    fontFamily: theme.font.secondaryExtraBold,
    fontWeight: '200',
    textAlign: 'center',
    flex: 1,
  },
  gesturesEnabled: true,
};

const navigationOptionsWithHeader = {
  ...navigationOptions,
  headerStyle: {
    ...navigationOptions.headerStyle,
    backgroundColor: theme.color.primary,
  },
  headerTintColor: theme.color.secondary,
};

const navigationOptionsTabBar = {
  tabBarOptions: {
    activeTintColor: theme.color.secondary,
    inactiveTintColor: theme.color.primaryLight,
    style: {
      borderTopWidth: 0,
      height: 57,
    },
    tabStyle: {
      backgroundColor: theme.color.primary,
      height: 57,
      paddingVertical: 5,
    },
    labelStyle: {
      fontSize: 13,
      fontFamily: theme.font.primaryBold
    }
  }
};

const OrderStack = createStackNavigator(
  {
    order_screen: {
      screen: Orders,
      navigationOptions: ({ navigation }) => ({
        // headerLeft: <MenuIcon navigation={navigation} />,
      }),
    },
  },
  {
    navigationOptions: navigationOptionsWithHeader,
  },
);

const HistoryStack = createStackNavigator(
  {
    history_screen: { screen: History },
  },
  {
    navigationOptions: navigationOptionsWithHeader,
  },
);

const NotificationStack = createStackNavigator(
  {
    notifications_screen: { screen: Notifications },
  },
  {
    navigationOptions: navigationOptionsWithHeader,
  },
);

const SettingStack = createStackNavigator(
  {
    settings_screen: { screen: Settings },
  },
  {
    navigationOptions: navigationOptionsWithHeader,
  },
);

const MainTab = createBottomTabNavigator(
  {
    order_stack: {
      screen: OrderStack,
      navigationOptions: {
        ...navigationOptionsTabBar,
        title: "Bưu gửi",
        tabBarIcon: ({ tintColor }) => 
          <Icon type="material-community" name="package" size={25} color={tintColor} />,
      }
    },
    history_stack: {
      screen: HistoryStack,
      navigationOptions: {
        ...navigationOptionsTabBar,
        title: "Lịch sử",
        tabBarIcon: ({ tintColor }) => 
          <Icon type="material-community" name="chart-arc" size={26} color={tintColor} />,
      }
    },
    notification_stack: {
      screen: NotificationStack,
      navigationOptions: {
        ...navigationOptionsTabBar,
        title: "Thông báo",
        tabBarIcon: ({ tintColor }) => 
          <NotificationIcon type="material" name="notifications" color={tintColor} />
      }
    },
    setting_stack: {
      screen: SettingStack,
      navigationOptions: {
        ...navigationOptionsTabBar,
        title: "Danh mục",
        tabBarIcon: ({ tintColor }) => 
          <Icon type="entypo" name="list" color={tintColor} />,
      }
    },
  }
);

const CreateOrderStack = createStackNavigator(
  {
    create_order_screen: {
      screen: CreateOrder,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <BackIcon navigation={navigation}/>,
        headerRight: <View />,
      }),
    },
    confirm_create_order_screen: {
      screen: ConfirmCreateOrder,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <BackIcon navigation={navigation}/>,
        headerRight: <View />,
      }),
    },
  },
  {
    navigationOptions: navigationOptionsWithHeader,
  },
);

const OrderDetailsStack = createStackNavigator(
  {
    order_details_screen: {
      screen: OrderDetails,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <BackIcon navigation={navigation}/>,
        headerRight: <View />,
      }),
    },
    messages_screen: {
      screen: Messages,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <BackIcon navigation={navigation}/>,
        headerRight: <View />,
      }),
    }
  },
  {
    navigationOptions: navigationOptionsWithHeader,
  },
);

const HomeStack = createStackNavigator(
  {
    main_tab: { screen: MainTab },
    create_order_stack: { screen: CreateOrderStack },
    order_details_stack: { screen: OrderDetailsStack },
  },
  {
    navigationOptions: {
      ...navigationOptions,
      header: null,
    },
    initialRouteName: 'main_tab',
  },
);

const AddressModalStack = createStackNavigator(
  {
    address_search: {
      screen: AddressSearch,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <BackIcon navigation={navigation}/>,
        headerRight: <View />,
      }),
    },
    address_picker: {
      screen: AddressPicker,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <BackIcon navigation={navigation}/>,
        headerRight: <View />,
      }),
    },
  },
  {
    navigationOptions: navigationOptionsWithHeader,
  },
);

const HomeStackWithModal = createStackNavigator(
  {
    home_stack: { screen: HomeStack },
    address_modal_stack: { screen: AddressModalStack },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

const AuthStack = createStackNavigator(
  {
    authentication_screen: { screen: Authentication, navigationOptions: { header: null } },
    welcome_screen: { screen: Welcome, navigationOptions: { header: null } },
  },
  {
    navigationOptions,
  },
);

const AppNavigator = createSwitchNavigator(
  {
    auth_stack: { screen: AuthStack },
    home_stack_with_modal: { screen: HomeStackWithModal },
  },
  {
    initialRouteName: 'auth_stack',
  },
);

export default AppNavigator;
