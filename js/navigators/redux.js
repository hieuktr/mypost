
import {
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';
import AppNavigator from './AppNavigator';

const navigationReducer = createNavigationReducer(AppNavigator);

const navigationReduxMiddleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav,
);

export { navigationReducer, navigationReduxMiddleware };
