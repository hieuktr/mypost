
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BackHandler, ToastAndroid } from 'react-native';
import { NavigationActions, DrawerActions } from 'react-navigation';
import { reduxifyNavigator } from 'react-navigation-redux-helpers';
import AppNavigator from './AppNavigator';

const NavigatorRedux = connect(state => ({ state: state.nav }))(reduxifyNavigator(AppNavigator, "root"));

class AppWithNavigationState extends Component {
  
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    const { dispatch, nav } = this.props;
    if ((nav.index === 0 && nav.routes[0].index > 0) ||
        (nav.index === 1) ||
        (nav.index === 2 && nav.routes[2].index > 0) ||
        (nav.index === 2 && nav.routes[2].routes[0].index !== 0) ||
        (nav.index === 2 && nav.routes[2].index === 0 && nav.routes[2].routes[0].routes[nav.routes[2].routes[0].index].index > 0)) {
      dispatch(NavigationActions.back());
    } else if (nav.index === 2 && nav.routes[2].index === 0 && nav.routes[2].routes[0].isDrawerOpen) {
      dispatch(DrawerActions.closeDrawer());
    } else {
      ToastAndroid.show('Press back button 2 times to exit.', ToastAndroid.SHORT);
      if (this._closedTime && new Date() - this._closedTime < 500) {
        BackHandler.exitApp();
      }
      this._closedTime = new Date();
    }
    return true;
  }

  render() {
    const { dispatch, nav } = this.props;
    return (
      <NavigatorRedux />
    );
  }
}

const mapStateToProps = state => ({
  nav: state.nav,
});

const mapDispatchToProps = dispatch => ({
  dispatch: action => dispatch(action),
});

export default connect(mapStateToProps, mapDispatchToProps)(AppWithNavigationState);
